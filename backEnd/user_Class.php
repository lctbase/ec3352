<?php

class User_Class{
    public function getUserDetail($db, $userid, &$userDetail){
        $val = 0;
        try{
            $getUser = mysqli_query($db, "SELECT * FROM user WHERE user_id = $userid");
            if($getUser){
                $userDetail = $getUser;
            }
        } catch (Exception $ex) {
            $val = 9999;
            LogMessage("2", '[User_Class]', '[getUserDetail]', "[ErrMsg]=>".$ex->getMessage());
        }
        return $val;
    }
    
    public function getCurrentOrderId($db, $userid, &$currentOrder){
        $val = 0;
        try{
            $getOrder = mysqli_query($db, "SELECT * FROM order_table WHERE user_id = $userid AND status = 1 AND delivery = 0 ORDER BY create_date");
            if($getOrder){
                $currentOrder = $getOrder;
            }
        } catch (Exception $ex) {
            $val = 9999;
            LogMessage("2", '[User_Class]', '[getCurrentOrderId]', "[ErrMsg]=>".$ex->getMessage());
        }
        return $val;
    }
    
    public function getOrderHistory($db, $userid, &$orderHistory){
        $val = 0;
        try{
            $getOrder = mysqli_query($db, "SELECT * FROM order_table WHERE user_id = $userid AND status = 1 AND delivery = 1 ORDER BY create_date");
            if($getOrder){
                $orderHistory = $getOrder;
            }
        } catch (Exception $ex) {
            $val = 9999;
            LogMessage("2", '[User_Class]', '[getOrderHistory]', "[ErrMsg]=>".$ex->getMessage());
        }
        return $val;
    }
    
    public function getUserRating($db, $userid, $RM_id, &$userRate){
        $val = 0;
        try{
            $select = mysqli_query($db, "SELECT * FROM rating WHERE RM_id = $RM_id and user_id = $userid");
            $rating = 0;
            if($select){
                $count = mysqli_num_rows($select);
                if($count != 0){
                    foreach($select as $row){
                        $rating = $rating + (int)$row["rating"];
                    }
                    $userRate = number_format(($rating / $count), 1);
                }else{
                    $userRate = 0;
                }
            }
        } catch (Exception $ex) {
            $val = 9999;
            LogMessage("2", '[User_Class]', '[getUserRating]', "[ErrMsg]=>".$ex->getMessage());
        }
        return $val;
    }
}

