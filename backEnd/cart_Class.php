<?php

class Cart_Class{
      
    public function getOrderId($db, $userid, &$orderResult){
        $val = 0;
        try{
            $getOrder = mysqli_query($db, "SELECT * FROM order_table WHERE user_id = $userid AND status = 0 ORDER BY create_date");
            if($getOrder){
                $orderResult = $getOrder;
            }
        } catch (Exception $ex) {
            $val = 9999;
            LogMessage("2", '[Cart_Class]', '[getOrderId]', "[ErrMsg]=>".$ex->getMessage());
        }
        return $val;
    }
    
    public function getCartDetail($db, $orderid, &$cartResult){
        $val = 0;
        try{
            $getCart = mysqli_query($db, "SELECT * FROM order_detail WHERE order_id = $orderid");
            if($getCart){
                $i = 0;
                foreach ($getCart as $row){
                    if(strlen((string)$row["product_id"]) == 6){
                        $select1 = mysqli_query($db, "SELECT * FROM customize WHERE product_id = ".$row["product_id"]);
                        if($select1){
                            $cartResult[$i] = mysqli_fetch_array($select1);
                            $cartResult[$i]["orderDetailID"] = $row["order_detail_id"];
                            $cartResult[$i]["orderQuantity"] = $row["quantity"];
                            $i++;
                        }
                    }else{
                        $select2 = mysqli_query($db, "SELECT * FROM ready_made WHERE product_id = ".$row["product_id"]);
                        if($select2){
                            $cartResult[$i] = mysqli_fetch_array($select2);
                            $cartResult[$i]["orderDetailID"] = $row["order_detail_id"];
                            $cartResult[$i]["orderQuantity"] = $row["quantity"];
                            foreach ($select2 as $row2){
                                if($row2["product_stock"] > 0){
                                    $cartResult[$i]["Status"] = "Available: ".$row2["product_stock"];
                                }else{
                                    $cartResult[$i]["Status"] = "Sold Out.";
                                }
                            }
                            $i++;
                        }
                    }
                }
            }
        } catch (Exception $ex) {
            $val = 9999;
            LogMessage("2", '[Cart_Class]', '[getCartDetail]', "[ErrMsg]=>".$ex->getMessage());
        }
        return $val;
    }   
    
    public function getCustomizeDetail($db, $productid, &$customizeDetail){
        $val = 0;
        try{
            $getProduct = mysqli_query($db, "SELECT * FROM customize WHERE product_id = $productid");
            if($getProduct){
                foreach($getProduct as $row){
                    $selectSleeve = mysqli_query($db, "SELECT * FROM sleeve WHERE sleeve_id = ".$row["sleeve_id"]);
                    $selectButton = mysqli_query($db, "SELECT * FROM button WHERE button_id = ".$row["button_id"]);
                    $selectCollar = mysqli_query($db, "SELECT * FROM collar WHERE collar_id = ".$row["collar_id"]);
                    $selectBack = mysqli_query($db, "SELECT * FROM back WHERE back_id = ".$row["back_id"]);
                    $selectColor = mysqli_query($db, "SELECT * FROM color WHERE color_id = ".$row["color_id"]);
                }
                foreach($selectSleeve as $row){$sleeve = $row["sleeve_name"];}
                foreach($selectButton as $row){$button = $row["button_img"];}
                foreach($selectCollar as $row){$collar = $row["collar_img"];}
                foreach($selectBack as $row){$back = $row["back_img"];}
                foreach($selectColor as $row){$color = $row["color_name"];}
                
                $customizeDetail["sleeve"] = $sleeve;
                $customizeDetail["button"] = $button;
                $customizeDetail["collar"] = $collar;
                $customizeDetail["back"] = $back;
                $customizeDetail["color"] = $color;
            }
        } catch (Exception $ex) {
            $val = 9999;
            LogMessage("2", '[Cart_Class]', '[getOrderId]', "[ErrMsg]=>".$ex->getMessage());
        }
        return $val;
    }
}

