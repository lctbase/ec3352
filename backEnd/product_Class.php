<?php

class Product_Class{
      
    public function getProductList($db, &$productResult, &$getList, $startfrom, $result_per_page){
        $val = 0;
        try{
            $get = mysqli_query($db, "SELECT DISTINCT(RM_id) FROM ready_made ORDER BY RM_id ASC LIMIT $startfrom, $result_per_page");
            $getList = mysqli_query($db, "SELECT DISTINCT(RM_id) FROM ready_made");
            if($get){
                $i = 0;
                foreach ($get as $row){
                    $select = mysqli_query($db, "SELECT * FROM ready_made WHERE RM_id = ".$row["RM_id"]."");
                    if($select){
                        $productResult[$i] = mysqli_fetch_array($select);
                        $productResult[$i]["Rate"] = Product_Class::getProductRating($db, $row["RM_id"]);
                        $i++;
                    }
                }
            }
        } catch (Exception $ex) {
            $val = 9999;
            LogMessage("2", '[Product_Class]', '[getProductList]', "[ErrMsg]=>".$ex->getMessage());
        }
        return $val;
    }
    
    public function getSearchList($db, $keyword, &$productResult, &$getList, $startfrom, $result_per_page){
        $val = 0;
        try{
            // add search keyword frequency
            $selectKW = mysqli_query($db, "SELECT * FROM search WHERE keyword = '$keyword'");
            if($selectKW){
                if(mysqli_num_rows($selectKW) != 0){
                    $updateKW = mysqli_query($db, "UPDATE search SET times = times + 1 WHERE keyword = '$keyword'");

                }else{
                    $insertKW = mysqli_query($db, "INSERT INTO search (keyword, times) VALUES ('$keyword', '1')");
                }
            }
            
            // get search result
            $get = mysqli_query($db, "SELECT DISTINCT(RM_id) FROM ready_made WHERE product_name LIKE '%".$keyword."%' ORDER BY RM_id ASC LIMIT $startfrom, $result_per_page");
            $getList = mysqli_query($db, "SELECT DISTINCT(RM_id) FROM ready_made WHERE product_name LIKE '%".$keyword."%'");
            if($get){
                $i = 0;
                foreach ($get as $row){
                    $select = mysqli_query($db, "SELECT * FROM ready_made WHERE RM_id = ".$row["RM_id"]."");
                    if($select){
                        $productResult[$i] = mysqli_fetch_array($select);
                        $productResult[$i]["Rate"] = Product_Class::getProductRating($db, $row["RM_id"]);
                        $i++;
                    }
                }
            }
        } catch (Exception $ex) {
            $val = 9999;
            LogMessage("2", '[Product_Class]', '[getSearchList]', "[ErrMsg]=>".$ex->getMessage());
        }
    }
    
    public function getProductRating($db, $id){
        $val = 0;
        try{
            $select = mysqli_query($db, "SELECT * FROM rating WHERE RM_ID = ".$id);
            if($select){
                $count = mysqli_num_rows($select);
                if($count == 0){
                    return "No Rating";
                }else{
                    $total = 0;
                    foreach ($select as $row){
                        $total = $total + (int)$row['rating'];
                    }
                    $val = $total / $count;
                }
            }
        } catch (Exception $ex) {
            $val = 9999;
            LogMessage("2", '[Product_Class]', '[getProductRating]', "[ErrMsg]=>".$ex->getMessage());
        }
        return $val;
    }
    
    public function getProductDetail($db, $RM_ID, &$productResult, &$productImg){
        $val = 0;
        try{
            $addView = mysqli_query($db, "UPDATE ready_made SET view = view + 1 WHERE RM_ID = ".$RM_ID);
            
            $select = mysqli_query($db, "SELECT * FROM ready_made WHERE RM_ID = ".$RM_ID);
            if($select){
                $productResult = $select;
            }
            foreach ($select as $row){
                $productImg = $row["product_img"];
            }
        } catch (Exception $ex) {
            $val = 9999;
            LogMessage("2", '[Product_Class]', '[getProductRating]', "[ErrMsg]=>".$ex->getMessage());
        }
        return $val;
    }
    
    public function getCustomizeList($db, &$sleeveList, &$colorList, &$collarList, &$buttonList, &$backList){
        $val = 0;
        try{
            $selectSleeve = mysqli_query($db, "SELECT * FROM sleeve");
            if($selectSleeve){
                $sleeveList = $selectSleeve;
            }
            
            $selectColor = mysqli_query($db, "SELECT * FROM color");
            if($selectColor){
                $colorList = $selectColor;
            }
            
            $selectCollar = mysqli_query($db, "SELECT * FROM collar");
            if($selectCollar){
                $collarList = $selectCollar;
            }
            
            $selectButton = mysqli_query($db, "SELECT * FROM button");
            if($selectButton){
                $buttonList = $selectButton;
            }
            
            $selectBack = mysqli_query($db, "SELECT * FROM back");
            if($selectBack){
                $backList = $selectBack;
            }
        } catch (Exception $ex) {
            $val = 9999;
            LogMessage("2", '[Product_Class]', '[getCustomizeList]', "[ErrMsg]=>".$ex->getMessage());
        }
    }
}

