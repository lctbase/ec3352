<?php

class Admin_Class{
      
    public function mostSearch($db, &$searchResult){
        $val = 0;
        try{
            $select = mysqli_query($db, "SELECT * FROM search ORDER BY times DESC");
            if($select){
                $searchResult = $select;
            }
        } catch (Exception $ex) {
            $val = 9999;
            LogMessage("2", '[Admin_Class]', '[mostSearch]', "[ErrMsg]=>".$ex->getMessage());
        }
        return $val;
    }
    
    public function mostView($db, &$viewResult){
        $val = 0;
        try{
            $select = mysqli_query($db, "SELECT DISTINCT(RM_id), view, product_name FROM ready_made WHERE view != 0 ORDER BY view DESC");
            if($select){
                $viewResult = $select;
            }
        } catch (Exception $ex) {
            $val = 9999;
            LogMessage("2", '[Admin_Class]', '[mostView]', "[ErrMsg]=>".$ex->getMessage());
        }
        return $val;
    }
    
    public function mostOrder($db, &$orderResult){
        $val = 0;
        try{
            $select = mysqli_query($db, "SELECT COUNT(order_detail_id), product_id FROM order_detail GROUP BY product_id ORDER BY COUNT(order_detail_id) DESC");
            if($select){
                $i = 0;
                $orderResult = mysqli_fetch_all($select);
                foreach($select as $row){
                    $getProduct = mysqli_query($db, "SELECT * FROM ready_made WHERE product_id = ".$row["product_id"]);
                    foreach($getProduct as $row2){$rm = $row2["RM_id"]; $name = $row2["product_name"]; $size = $row2["size"];}
                    $orderResult[$i]["view"] = $row["COUNT(order_detail_id)"];
                    $orderResult[$i]["RM_id"] = $rm;
                    $orderResult[$i]["size"] = $size;
                    $orderResult[$i]["product_name"] = $name;
                    $i++;
                }
            }
        } catch (Exception $ex) {
            $val = 9999;
            LogMessage("2", '[Admin_Class]', '[mostOrder]', "[ErrMsg]=>".$ex->getMessage());
        }
        return $val;
    }
    
    public function mostOrderTgt($db, &$orderTgtResult){
        $val = 0;
        try{
            $select = mysqli_query($db, "SELECT COUNT(order_id), order_tgt FROM order_table WHERE order_tgt != '' GROUP BY order_tgt ORDER BY COUNT(order_id) DESC");
            if($select){
                $orderTgtResult = $select;
            }
        } catch (Exception $ex) {
            $val = 9999;
            LogMessage("2", '[Admin_Class]', '[mostOrder]', "[ErrMsg]=>".$ex->getMessage());
        }
        return $val;
    }
    
    public function getDetail($db, $RM_id, &$data){
        $val = 0;
        try{
            $select = mysqli_query($db, "SELECT DISTINCT(RM_id), product_name FROM ready_made WHERE RM_id = $RM_id");
            if($select){
                $data = $select;
            }
        } catch (Exception $ex) {
            $val = 9999;
            LogMessage("2", '[Admin_Class]', '[getDetail]', "[ErrMsg]=>".$ex->getMessage());
        }
        return $val;
    }
}
