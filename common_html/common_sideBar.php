<?php ?>

<div class="left-sidebar">
    <h2>Our Products</h2>
    <div class="panel-group category-products" id="accordian"><!--category-productsr-->
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordian" href="shop_now.php">
                        <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                        <a href="shop_now.php">Shop Now</a>
                    </a>
                </h4>
            </div>

        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordian" href="customize_now.php">
                        <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                        <a href="customize_now.php">Customize Now</a>
                    </a>
                </h4>
            </div>
        </div>
        <div class="panel panel-default">
            <form action="shop_now.php" method="get">
                <center>
                    <input type="text" name="search" id="search" placeholder="Search for Products" style="width: 75%">
                    <button type="submit"><i class="fa fa-search"></i></button>
                </center>
            </form>
        </div>
    </div>
</div>