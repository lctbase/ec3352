<?php ?>
<div class="header_top"><!--header_top-->
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <div class="contactinfo">
                    <ul class="nav nav-pills">

                    </ul>
                </div>
            </div>
        </div>
    </div>
</div><!--/header_top-->

<div class="header-middle"><!--header-middle-->
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <div class="logo pull-left">
                    <a href="index.php"><img src="" alt="" /></a>
                </div>

            </div>
            <div class="col-sm-8">
                <div class="shop-menu pull-right">
                    <ul class="nav navbar-nav">
                        <li><a href="shopping_cart.php"><i class=" "></i><?php echo $btn_cart ?></a></li>
                        <li><a href="user_profile.php"><i class=" "></i><?php echo $btn_profile ?></a></li>
                        <li><a href="logout.php?btn=<?= $btn; ?>"><i class="fa fa-lock"></i><?php echo $btn ?></a></li>
                        <li><a href="register.php"><?php echo $btn_register ?></a></li>

                    </ul>
                </div>
            </div>
        </div>
    </div>
</div><!--/header-middle-->

<div class="header-bottom"><!--header-bottom-->
    <div class="container">
        <div class="row">
            <div class="col-sm-9">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <div class="mainmenu pull-left">
                    <ul class="nav navbar-nav collapse navbar-collapse">
                        <li><a href="index.php" class="active">Home</a></li>
                        <li><a href="about_us.php" class="active">About Us</a></li>
                        <li><a href="contact_us.php">Contact Us</a></li>

                      

                    </ul>
                </div>
            </div>

        </div>
    </div>
</div><!--/header-bottom-->