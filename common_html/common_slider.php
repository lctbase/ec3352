<?php ?>

<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div id="slider-carousel" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#slider-carousel" data-slide-to="0" class="active"></li>
                    <li data-target="#slider-carousel" data-slide-to="1"></li>
                    <li data-target="#slider-carousel" data-slide-to="2"></li>
                </ol>

                <div class="carousel-inner">
                    <div class="item active">
                        <div class="col-sm-6">
                            <h1><span>Your Shirt</span><br>Josin Fashion Men's False Two Pieces Shirt</h1>
                            <h2></h2>
                            <p></p>
                            <a href="shop_now.php"><button type="button" class="btn btn-default get">Check it now</button></a>
                        </div>
                        <div class="col-sm-6">
                            <img src="img/ready_made/b0008_black.jpg" class="girl img-responsive" height="300" width="298">
                        </div>
                    </div>
                    <div class="item">
                        <div class="col-sm-6">
                            <h1><span>Your Shirt</span><br>Josin New Floral Korean Shirt Fashion</h1>
                            <h2></h2>
                            <p></p>
                            <a href="shop_now.php"><button type="button" class="btn btn-default get">Check it now</button></a>
                        </div>
                        <div class="col-sm-6">
                            <img src="img/ready_made/b0009_pink.jpg" class="girl img-responsive" height="300" width="298">
                        </div>
                    </div>
                    <div class="item">
                        <div class="col-sm-6">
                            <h1><span>Your Shirt</span><br>Rainny Mens Oxford Formal Casual Suits</h1>
                            <h2></h2>
                            <p></p>
                            <a href="shop_now.php"><button type="button" class="btn btn-default get">Check it now</button></a>
                        </div>
                        <div class="col-sm-6">
                            <img src="img/ready_made/b0011_red.jpg" class="girl img-responsive" height="300" width="298">
                        </div>
                    </div>


                </div>

                <a href="#slider-carousel" class="left control-carousel hidden-xs" data-slide="prev">
                    <i class="fa fa-angle-left"></i>
                </a>
                <a href="#slider-carousel" class="right control-carousel hidden-xs" data-slide="next">
                    <i class="fa fa-angle-right"></i>
                </a>
            </div>

        </div>
    </div>
</div>
