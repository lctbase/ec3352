<?php 
    if(isset($_GET["sleeve"])){
        $sleeve = $_GET["sleeve"];
        $button = $_GET["button"];
        $collar = $_GET["collar"];
        $back = $_GET["back"];
        $color = $_GET["color"];
    }else{
        header('Location: index.php');
    }
?>
<div class="form">
    <div class="panel-heading">
        <h1 class="title">Preview</h1>
    </div>
    <div class="column" style="width: 45%; float: left;">
        <img id="sleeve_img" src="img/customize/<?=$sleeve?>-<?=$color?>.png" style="position: absolute" >
        <img id="button_img" src="img/customize/<?=$button?>" style="position: absolute">
        <img id="collar_img" src="img/customize/<?=$collar?>" style="position: absolute">
        <img id="border" src="img/customize/none.png" style="position: relative">
    </div>
    <div class="column" style="width: 45%; float: right;">
        <img id="back_shirt" src="img/customize/back-<?=$sleeve?>-<?=$color?>.png" style="position: absolute">
        <img id="back_img" src="img/customize/<?=$back?>" style="position: absolute">
    </div>
</div>

