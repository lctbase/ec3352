<?php
//include("config.php");
include('session.php');

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $userid = rand(100000, 999999);
    $fname = $_POST['fname'];
    $lname = $_POST['lname'];
    $email = $_POST['useremail'];
    $address = $_POST['address'];
    $password = $_POST['userpassword'];
    $role_id = 2;

    $insert = mysqli_query($db, "INSERT INTO user(user_id,user_password,user_fname,user_lname,user_email,user_address,role_id)
		VALUES ('$userid','$password','$fname','$lname','$email','$address','$role_id')");
    if ($insert) {
        echo "<script type='text/javascript'>alert('Register Successfully.')
                        window.location.replace(\"login_user.php\");</script>";
    } else {
        echo "<script type='text/javascript'>alert('Register Failed.')</script>";
    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Your Shirt</title>
        <?php include 'common_html/common_css.php' ?>
    </head><!--/head-->

    <body>
        <header id="header"><!--header-->
            <?php include 'common_html/common_header.php' ?>
        </header><!--/header-->

        <section id="" style="min-height: 400px"><!--form-->
            <div class="container">
                <div class="row">
                    <div class="col-sm-4 col-sm-offset-1">
                        <div class="login-form"><!--login form-->
                            <h2>Register your account</h2>
                            <form action="register.php" method="post">
                                <input type="text" placeholder="First Name" name="fname" required="true"/>
                                <input type="text" placeholder="Last Name" name="lname" required="true"/>
                                <input type="email" placeholder="Email" name="useremail" required="true"/>
                                <input type="text" placeholder="Address" name="address" required="true"/>
                                <input type="password" placeholder="Password" name="userpassword" required="true"/>


                                <button type="Submit" class="btn btn-default" style="width: 100%">Register</button>
                            </form>
                        </div><!--/login form-->
                    </div>

                </div>

            </div>
        </section>


        <footer id="footer"><!--Footer-->
            <div class="footer-top">
                <div class="container">

                </div>
            </div>

            <div class="footer-widget">
                <div class="container">

                </div>
            </div>



        </footer><!--/Footer-->



        <script src="js&css/jquery.js"></script>
        <script src="js&css/bootstrap.min.js"></script>
        <script src="js&css/jquery.scrollUp.min.js"></script>
        <script src="js&css/price-range.js"></script>
        <script src="js&css/jquery.prettyPhoto.js"></script>
        <script src="js&css/main.js"></script>
        <script src="js&css/Chart.js"></script>
        <script src="js&css/chart.js-php.js"></script>
    </body>
</html>

