<?php
include('session.php');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Your Shirt</title>

        <?php include 'common_html/common_css.php' ?>
        <link href="js&css/bootstrap.min.css" rel="stylesheet">
        <link href="js&css/font-awesome.min.css" rel="stylesheet">
        <link href="js&css/prettyPhoto.css" rel="stylesheet">
        <link href="js&css/price-range.css" rel="stylesheet">
        <link href="js&css/animate.css" rel="stylesheet">
        <link href="js&css/main.css" rel="stylesheet">
        <link href="js&css/responsive.css" rel="stylesheet">


        <!--<link rel="shortcut icon" href="images/ico/favicon.ico">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">-->
    </head><!--/head-->

    <body>
        <header id="header"><!--header-->
            <?php include 'common_html/common_header.php' ?>
        </header><!--/header-->

        <section id="slider"><!--slider-->
            <?php include 'common_html/common_slider.php' ?>
        </section><!--/slider-->

        <section>
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <?php include 'common_html/common_sideBar.php' ?>
                    </div>
                    <div class="col-sm-9 padding-right"><!-- PS buat sini-->
                        <div class="features_items">
                            <h2 class="title text-center">Shirts</h2>
                            <div class="col-sm-4">
                                <div class="product-image-wrapper">
                                    <div class="single-products">
                                        <div class="productinfo text-center">
                                            <img src="img/ready_made/Autumn_Style_Man.jpg" alt="" />
                                            <a href="product_detail.php?rmid=1&product=Autumn Style Shirt For Men Slim Business Casual Shirt Blue"><h2>Autumn Style Shirt</h2></a>
                                            <p>Autumn Style Shirt For Men Slim Business Casual Shirt Blue</p>

                                        </div>
                                        
                                    </div>
                                    <div class="choose">
                                        <ul class="nav nav-pills nav-justified">

                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="product-image-wrapper">
                                    <div class="single-products">
                                        <div class="productinfo text-center">
                                            <img src="img/ready_made/Class_Blue_Man.jpg" alt="" />
                                            <a href="product_detail.php?rmid=2&product=Autumn Style Shirt For Men Slim Business Casual Shirt Blue"><h2>Classic Casual Shirt</h2></a>
                                            <p>Classic Casual Business Slim Shirt For Men Blue</p>

                                        </div>
                                        
                                    </div>
                                    <div class="choose">
                                        <ul class="nav nav-pills nav-justified">

                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="product-image-wrapper">
                                    <div class="single-products">
                                        <div class="productinfo text-center">
                                            <img src="img/ready_made/b0003_blue.jpg" alt=""   />
                                            <a href="product_detail.php?rmid=3&product=Betrare Fashion Shirt Korean Style Slim Casual Floral Shirt For Men Blue"><h2>Betrare Fashion Korean Style</h2></a>
                                            <p>Betrare Fashion Shirt Korean Style Slim Casual Floral Shirt For Men Blue</p>

                                        </div>
                                        
                                    </div>
                                    <div class="choose">
                                        <ul class="nav nav-pills nav-justified">

                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="product-image-wrapper">
                                    <div class="single-products">
                                        <div class="productinfo text-center">
                                            <img src="img/ready_made/b0004_blue.jpg" alt="" />
                                            <a href="product_detail.php?rmid=10&product=New Fall FashionCasual Digital Print Lightning FeatherFor Male 2017"><h2>Fashion Casual Digital Print Shirt</h2></a>
                                            <p>New Fall Fashion Casual Digital Print Lightning Feather For Male 2017</p>

                                        </div>
                                        
                                    </div>
                                    <div class="choose">
                                        <ul class="nav nav-pills nav-justified">

                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="product-image-wrapper">
                                    <div class="single-products">
                                        <div class="productinfo text-center">
                                            <img src="img/ready_made/b0002_white.jpg" alt="" />
                                            <a href="product_detail.php?rmid=7&product=Betrare Fashion Shirt Korean Style Plain Shirt For Men Slim Business Casual White"><h2>Betrare Fashion Shirt Korean Style</h2></a>
                                            <p>Betrare Fashion Shirt Korean Style Plain Shirt For Men Slim Business Casual White</p>

                                        </div>
                                        
                                    </div>
                                    <div class="choose">
                                        <ul class="nav nav-pills nav-justified">

                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="product-image-wrapper">
                                    <div class="single-products">
                                        <div class="productinfo text-center">
                                            <img src="img/ready_made/b0011_black.jpg" alt="" />
                                            <a href="product_detail.php?rmid=26&product=Rainny Mens Oxford Formal Casual Suits Slim Fit Tee Dress Blouse Top Black"><h2>Rainny Mens Oxford Formal Casual Suits</h2></a>
                                            <p>Rainny Mens Oxford Formal Casual Suits Slim Fit Tee Dress Blouse Top Black</p>

                                        </div>
                                        
                                    </div>
                                    <div class="choose">
                                        <ul class="nav nav-pills nav-justified">

                                        </ul>
                                    </div>
                                </div>
                            </div>

                        </div><!--features_items-->



                        

                    </div>


                </div>
            </div>
        </section>

        <footer id="footer"><!--Footer-->
            <?php include 'common_html/common_footer.php' ?>
        </footer><!--/Footer-->



        <script src="js&css/jquery.js"></script>
        <script src="js&css/bootstrap.min.js"></script>
        <script src="js&css/jquery.scrollUp.min.js"></script>
        <script src="js&css/price-range.js"></script>
        <script src="js&css/jquery.prettyPhoto.js"></script>
        <script src="js&css/main.js"></script>
        <script src="js&css/Chart.js"></script>
        <script src="js&css/chart.js-php.js"></script>
    </body>
    
</html>
