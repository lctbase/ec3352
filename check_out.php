<?php
include('session.php');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Your Shirt</title>

        <?php include 'common_html/common_css.php' ?>
    </head><!--/head-->

    <body>
        <header id="header"><!--header-->
            <?php include 'common_html/common_header.php' ?>
        </header><!--/header-->

        <section id="slider"><!--slider-->
            <?php include 'common_html/common_slider.php' ?>
        </section><!--/slider-->

        <section>
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <?php include 'common_html/common_sideBar.php' ?>
                    </div>
                    <div class="col-sm-9 padding-right"><!-- PS buat sini-->
                        <div class="features_items">
                            <h2 class="title text-center">Shirts</h2>
                            <div class="col-sm-4">
                                <div class="product-image-wrapper">
                                    <div class="single-products">
                                        <div class="productinfo text-center">
                                            <img src="images/home/ec1.jpeg" alt="" />
                                            <h2>Name</h2>
                                            <p>Description</p>

                                        </div>
                                        <div class="product-overlay">
                                            <div class="overlay-content">

                                                <a href="#" class="btn btn-default add-to-cart">Check for detail</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="choose">
                                        <ul class="nav nav-pills nav-justified">

                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="product-image-wrapper">
                                    <div class="single-products">
                                        <div class="productinfo text-center">
                                            <img src="images/home/ec2.jpeg" alt="" />
                                            <h2>Name</h2>
                                            <p>Description</p>

                                        </div>
                                        <div class="product-overlay">
                                            <div class="overlay-content">

                                                <a href="#" class="btn btn-default add-to-cart">Check for detail</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="choose">
                                        <ul class="nav nav-pills nav-justified">

                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="product-image-wrapper">
                                    <div class="single-products">
                                        <div class="productinfo text-center">
                                            <img src="images/home/ec3.jpeg" alt="" />
                                            <h2>Name</h2>
                                            <p>Description</p>

                                        </div>
                                        <div class="product-overlay">
                                            <div class="overlay-content">

                                                <a href="#" class="btn btn-default add-to-cart">Check for detail</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="choose">
                                        <ul class="nav nav-pills nav-justified">

                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="product-image-wrapper">
                                    <div class="single-products">
                                        <div class="productinfo text-center">
                                            <img src="images/home/wd1.jpeg" alt="" />
                                            <h2>Name</h2>
                                            <p>Description</p>

                                        </div>
                                        <div class="product-overlay">
                                            <div class="overlay-content">

                                                <a href="#" class="btn btn-default add-to-cart">Check for detail</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="choose">
                                        <ul class="nav nav-pills nav-justified">

                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="product-image-wrapper">
                                    <div class="single-products">
                                        <div class="productinfo text-center">
                                            <img src="images/home/wd2.jpeg" alt="" />
                                            <h2>Name</h2>
                                            <p>Description</p>

                                        </div>
                                        <div class="product-overlay">
                                            <div class="overlay-content">

                                                <a href="#" class="btn btn-default add-to-cart">Check for detail</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="choose">
                                        <ul class="nav nav-pills nav-justified">

                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="product-image-wrapper">
                                    <div class="single-products">
                                        <div class="productinfo text-center">
                                            <img src="images/home/wd3.jpeg" alt="" />
                                            <h2>Name</h2>
                                            <p>Description</p>

                                        </div>
                                        <div class="product-overlay">
                                            <div class="overlay-content">

                                                <a href="#" class="btn btn-default add-to-cart">Check for detail</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="choose">
                                        <ul class="nav nav-pills nav-justified">

                                        </ul>
                                    </div>
                                </div>
                            </div>

                        </div><!--features_items-->



                        <div class="recommended_items"><!--recommended_items-->
                            <h2 class="title text-center">Recommended Items</h2>

                            <div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="item active">
                                        <div class="col-sm-4">
                                            <div class="product-image-wrapper">
                                                <div class="single-products">
                                                    <div class="productinfo text-center">


                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="product-image-wrapper">
                                                <div class="single-products">
                                                    <div class="productinfo text-center">


                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="product-image-wrapper">
                                                <div class="single-products">
                                                    <div class="productinfo text-center">


                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="col-sm-4">
                                            <div class="product-image-wrapper">
                                                <div class="single-products">
                                                    <div class="productinfo text-center">


                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="product-image-wrapper">
                                                <div class="single-products">
                                                    <div class="productinfo text-center">


                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="product-image-wrapper">
                                                <div class="single-products">
                                                    <div class="productinfo text-center">


                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <a class="left recommended-item-control" href="#recommended-item-carousel" data-slide="prev">
                                    <i class="fa fa-angle-left"></i>
                                </a>
                                <a class="right recommended-item-control" href="#recommended-item-carousel" data-slide="next">
                                    <i class="fa fa-angle-right"></i>
                                </a>
                            </div>
                        </div><!--/recommended_items-->

                    </div>


                </div>
            </div>
        </section>

        <footer id="footer"><!--Footer-->
            <?php include 'common_html/common_footer.php' ?>
        </footer><!--/Footer-->



        <script src="js&css/jquery.js"></script>
        <script src="js&css/bootstrap.min.js"></script>
        <script src="js&css/jquery.scrollUp.min.js"></script>
        <script src="js&css/price-range.js"></script>
        <script src="js&css/jquery.prettyPhoto.js"></script>
        <script src="js&css/main.js"></script>
        <script src="js&css/Chart.js"></script>
        <script src="js&css/chart.js-php.js"></script>
    </body>
    
</html>
