<?php
include('session.php');
include_once 'backEnd/product_Class.php';

$result_per_page = 5;
if(isset($_GET["page"])) {
    $page = $_GET["page"];
} else {
    $page = 1;
}
$startfrom = ($page - 1) * $result_per_page;
$productClass = new Product_Class();

if (isset($_GET["search"])) {
    $keyword = $_GET["search"];
    if($keyword != "" && $keyword != NULL){
        $productClass->getSearchList($db, $keyword, $productResult, $getList, $startfrom, $result_per_page);
    }else{
        $keyword = "";
        $productClass->getProductList($db, $productResult, $getList, $startfrom, $result_per_page);
    }
}else{
    $keyword = "";
    $productClass->getProductList($db, $productResult, $getList, $startfrom, $result_per_page);
}

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Your Shirt | Shop Now</title>
        <?php include 'common_html/common_css.php' ?>
    </head><!--/head-->

    <body>
        <header id="header"><!--header-->
            <?php include 'common_html/common_header.php' ?>
        </header><!--/header-->

        <section id="slider"><!--slider-->
            <?php include 'common_html/common_slider.php' ?>
        </section><!--/slider-->

        <section>
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <?php include 'common_html/common_sideBar.php' ?>
                    </div>
                    <div class="col-sm-9 padding-right"><!-- PS buat sini-->
                        <div class="features_items">
                            <h2 class="title text-center">SHOP NOW</h2>
                            <div class="table-responsive cart_info">
                                <div class="form">
                                    <form action="#" method="post">
                                        <?php
                                            if($keyword != ""){
                                                echo "<h4>Search: $keyword </h4>";
                                            }
                                        ?>
                                        <table class="table table-condensed table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th></th><th style="width: 45%;">Product Name</th>
                                                    <th>Price (RM)</th><th style="text-align: center;">Rating</th><th></th>
                                                </tr>
                                            </thead>    
                                            <tbody>

                                                <?php
                                                if (isset($productResult)) {
                                                    foreach ($productResult as $row) {
                                                        echo "<tr>"
                                                        . "<td><img src=\"img/ready_made/" . $row["product_img"] . "\" style=\"width:100px;\"></td>"
                                                        . "<td>" . $row["product_name"] . "</td>"
                                                        . "<td>" . number_format($row["product_price"], 2) . "</td>"
                                                        . "<td style=\"text-align: center;\">" . $row["Rate"] . "</td>"
                                                        . "<td><a class=\"btn\" href=\"product_detail.php?rmid=" . $row["RM_id"] . "&product=" . $row["product_name"] . "\">See Details</a></td>"
                                                        . "</tr>";
                                                    }
                                                }
                                                ?>
                                            


                                            </tbody>
                                        </table>
                                                <?php
                                                       //$total = $getList;
                                                        if($getList){
                                                            $count = mysqli_num_rows($getList);
                                                            $totalpages=ceil($count/$result_per_page);
                                                            
                                                                for ($i=1; $i<=$totalpages; $i++) { 
                                                                    echo "<a href='shop_now.php?page=".$i."&search=$keyword'>".$i."</a> "; 
                                                                }; 
                                                        }else{
                                                            echo "error";
                                                        }
                                                ?>
                                               
                                        
                                    </form>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>

        <footer id="footer"><!--Footer-->
            <?php include 'common_html/common_footer.php' ?>
        </footer><!--/Footer-->



        <script src="js&css/jquery.js"></script>
        <script src="js&css/bootstrap.min.js"></script>
        <script src="js&css/jquery.scrollUp.min.js"></script>
        <script src="js&css/price-range.js"></script>
        <script src="js&css/jquery.prettyPhoto.js"></script>
        <script src="js&css/main.js"></script>
        <script src="js&css/Chart.js"></script>
        <script src="js&css/chart.js-php.js"></script>

    </body>

</html>
