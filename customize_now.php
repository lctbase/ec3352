<?php
include('session.php');

//PS TONG
include_once 'backEnd/product_Class.php';
$productClass = new Product_Class();
$productClass->getCustomizeList($db, $sleeveList, $colorList, $collarList, $buttonList, $backList);
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Your Shirt | Customize Now</title>

        <?php include 'common_html/common_css.php' ?>
    </head><!--/head-->

    <body>
        <header id="header"><!--header-->
            <?php include 'common_html/common_header.php' ?>
        </header><!--/header-->

        <section id="slider"><!--slider-->
            <?php include 'common_html/common_slider.php' ?>
        </section><!--/slider-->

        <section>
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <?php include 'common_html/common_sideBar.php' ?>
                    </div>
                    <div class="col-sm-9 padding-right"><!-- PS buat sini-->
                        <div class="features_items">
                            <h2 class="title text-center">CUSTOMIZE NOW</h2>
                            <div class="shop-menu" style="margin-bottom: 10%;">
                                <ul class="nav navbar-nav">
                                    <li>Sleeve
                                        <select id="sleeve" onchange="loadPreview();">
                                            <?php 
                                                if(isset($sleeveList)){
                                                    foreach($sleeveList as $row){
                                                        echo "<option value=\"".$row["sleeve_name"]."\">".$row["sleeve_name"]."</option>";
                                                    }
                                                }
                                            ?>
                                        </select>
                                    </li>
                                    <li>Colour
                                        <select id="color" onchange="loadPreview();">
                                            <?php 
                                                if(isset($colorList)){
                                                    foreach($colorList as $row){
                                                        echo "<option value=\"".$row["color_name"]."\">".$row["color_name"]."</option>";
                                                    }
                                                }
                                            ?>
                                        </select>
                                    </li>
                                    <li>Collar
                                        <select id="collar" onchange="loadPreview();">
                                            <?php 
                                                if(isset($collarList)){
                                                    foreach($collarList as $row){
                                                        echo "<option value=\"".$row["collar_img"]."\">".$row["collar_name"]."</option>";
                                                    }
                                                }
                                            ?>
                                        </select>
                                    </li>
                                    <li>Button
                                        <select id="button" onchange="loadPreview();">
                                            <?php 
                                                if(isset($buttonList)){
                                                    foreach($buttonList as $row){
                                                        echo "<option value=\"".$row["button_img"]."\">".$row["button_name"]."</option>";
                                                    }
                                                }
                                            ?>
                                        </select>
                                    </li>
                                    <li>Back
                                        <select id="back" onchange="loadPreview();">
                                            <?php 
                                                if(isset($backList)){
                                                    foreach($backList as $row){
                                                        echo "<option value=\"".$row["back_img"]."\">".$row["back_name"]."</option>";
                                                    }
                                                }
                                            ?>
                                        </select>
                                    </li>
                                    <li>
                                        <select id="size">
                                            <option value="XS">XS</option>
                                            <option value="S">S</option>
                                            <option value="M">M</option>
                                            <option value="L">L</option>
                                            <option value="XL">XL</option>
                                        </select>
                                    </li>
                                    <li>
                                        <input type="button" class="btn" id="addCart_btn" value="Add to Cart" onclick="addToCart();">
                                    </li>
                                </ul>
                            </div>
                            
                            <div class="form">
                                <div class="panel-heading">
                                    <h4 class="title">Preview</h4>
                                </div>
                                <div class="column" style="width: 45%; float: left;">
                                    <img id="sleeve_img" src="img/customize/long-black.png" style="position: absolute" >
                                    <img id="button_img" src="img/customize/button-black.png" style="position: absolute">
                                    <img id="collar_img" src="img/customize/collar-black.png" style="position: absolute">
                                    <img id="border" src="img/customize/none.png" style="position: relative">
                                </div>
                                <div class="column" style="width: 45%; float: right;">
                                    <img id="back_shirt" src="img/customize/back-long-black.png" style="position: absolute">
                                    <img id="back_img" src="img/customize/back-center.png" style="position: absolute">
                                </div>
                            </div>
                        </div><!--features_items-->
                    </div>
                </div>
            </div>
        </section>

        <footer id="footer"><!--Footer-->
            <?php include 'common_html/common_footer.php' ?>
        </footer><!--/Footer-->



        <script src="js&css/jquery.js"></script>
        <script src="js&css/bootstrap.min.js"></script>
        <script src="js&css/jquery.scrollUp.min.js"></script>
        <script src="js&css/price-range.js"></script>
        <script src="js&css/jquery.prettyPhoto.js"></script>
        <script src="js&css/main.js"></script>
        <script src="js&css/Chart.js"></script>
        <script src="js&css/chart.js-php.js"></script>
        <script>
            function loadPreview(){
                var sleeve = document.getElementById("sleeve").value.toLowerCase();
                var button = document.getElementById("button").value;
                var collar = document.getElementById("collar").value;
                var back = document.getElementById("back").value;                
                var color = document.getElementById("color").value.toLowerCase();
                
                document.getElementById("sleeve_img").src = "img/customize/"+sleeve+"-"+color+".png";
                document.getElementById("button_img").src = "img/customize/"+button;
                document.getElementById("collar_img").src = "img/customize/"+collar;
                document.getElementById("back_shirt").src = "img/customize/back-"+sleeve+"-"+color+".png";
                document.getElementById("back_img").src = "img/customize/"+back;
            };
            
            function addToCart(){
                var login_id = "<?=(isset($login_session)? $login_session : "");?>";
                if(login_id !== ""){
                    var sleeve = document.getElementById("sleeve").value;
                    var button = document.getElementById("button").value;
                    var collar = document.getElementById("collar").value;
                    var back = document.getElementById("back").value;                
                    var color = document.getElementById("color").value;
                    var size = document.getElementById("size").value;

                    var form_data = new FormData();
                    form_data.append("sleeve", sleeve);
                    form_data.append("button", button);
                    form_data.append("collar", collar);
                    form_data.append("back", back);
                    form_data.append("color", color);
                    form_data.append("size", size);
                    form_data.append("action", "customize");

                    $.ajax({
                        url: 'api/CartApi.php',
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data,
                        type: 'post',
                        success: function(response){
                            var obj = jQuery.parseJSON(response);
                            if(obj.ErrCode === '0'){
                                alert("Add Successfully.");
                                location.href = "shopping_cart.php";
                            }else{
                                alert(obj.ErrCode+"-"+obj.ErrMsg);
                            }
                        }
                    });
                }else{
                    location.href = "login_user.php";
                }
            };
        </script>
    </body>
    
</html>
