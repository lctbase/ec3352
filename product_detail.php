<?php
include('session.php');


//PS Tong
include_once 'backEnd/product_Class.php';
$productClass = new Product_Class();
if(isset($_GET["rmid"])){
    $productName = $_GET["product"];
    $RM_ID = $_GET["rmid"];
    $productClass->getProductDetail($db, $RM_ID, $productResult, $productImg);
}else{
    header("Location: index.php");
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Your Shirt</title>
        
        <?php include 'common_html/common_css.php' ?>
    </head><!--/head-->

    <body>
        <header id="header"><!--header-->
            <?php include 'common_html/common_header.php' ?>
        </header><!--/header-->

        <section id="slider"><!--slider-->
            <?php include 'common_html/common_slider.php' ?>
        </section><!--/slider-->

        <section>
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <?php include 'common_html/common_sideBar.php' ?>
                    </div>
                    <div class="col-sm-9 padding-right"><!-- PS buat sini-->
                        <div style="float:left;width: 50%;">
                            <img src="img/ready_made/<?= $productImg; ?>" style="width: 100%;">
                        </div>
                        <div class="features_items" style="width:50%">
                            <div class="title">
                                <h2><?= $productName; ?></h2>
                            </div>
                            <div class="table-responsive cart_info">
                                <div class="form">
                                    <form action="" method="post">
                                        <table class="table table-condensed table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Available Size</th><th>Stock Left</th><th></th>
                                                </tr>
                                            </thead>    
                                            <tbody>
                                                <?php
                                                if (isset($productResult)) {
                                                    foreach ($productResult as $row) {
                                                        if($row["product_stock"] != 0){
                                                            echo "<tr>"
                                                            . "<td>".$row["size"]."</td>"
                                                            . "<td>".$row["product_stock"]."</td>"
                                                            . "<td><input type=\"checkbox\" name=\"productid\" value=\"".$row["product_id"]."\"</td>"
                                                            . "</tr>";
                                                        }
                                                    }
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                        <input type="button" id="submit" class="btn" value="Add To Cart" onclick="addToCart();">
                                        <input type="button" class="btn" value="Back" onclick="back();">
                                    </form>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>

        <footer id="footer"><!--Footer-->
            <?php include 'common_html/common_footer.php' ?>
        </footer><!--/Footer-->



        <script src="js&css/jquery.js"></script>
        <script src="js&css/bootstrap.min.js"></script>
        <script src="js&css/jquery.scrollUp.min.js"></script>
        <script src="js&css/price-range.js"></script>
        <script src="js&css/jquery.prettyPhoto.js"></script>
        <script src="js&css/main.js"></script>
        <script src="js&css/Chart.js"></script>
        <script src="js&css/chart.js-php.js"></script>
        
        <script>
            function back(){
                location.href = "shop_now.php";
            }
            
            function addToCart(){
                var login_id = "<?=(isset($login_session)? $login_session : "");?>";
                var checkbox = document.getElementsByName("productid");
                var count = 0;
                var product = [];
                var finalResponse = 0;
                for(var i = 0; i < checkbox.length; i++){
                    if(checkbox[i].checked){
                        product[count] = checkbox[i].value;
                        count++;
                    }
                }
                
                if(product.length === 0){
                    alert("No Item Selected");
                }else{
                    if(login_id !== ""){
                        for(var j = 0; j < product.length; j++){
                            var form_data = new FormData();
                            form_data.append("productid", product[j]);
                            form_data.append("action", "ready_made");

                            $.ajax({
                                url: 'api/CartApi.php',
                                cache: false,
                                contentType: false,
                                processData: false,
                                data: form_data,
                                type: 'post',
                                success: function(response){
                                    var obj = jQuery.parseJSON(response);
                                    if(obj.ErrCode !== '0'){
                                        finalResponse++;
                                    }
                                }
                            });
                        }
                        if(finalResponse === 0){
                            alert("Add Successfully.");
                            location.href = "shopping_cart.php";
                        }else{
                            alert("Add Failed.");
                        }
                    }else{
                        location.href = "login_user.php";
                    }
                }
            }
        </script>
    </body>
    
</html>
