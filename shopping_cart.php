<?php
include('session.php');
//check user login
if(!isset($login_session)){
    header('Location: login_user.php');
}
include_once 'backEnd/cart_Class.php';
$cartClass = new Cart_Class();
$cartClass->getOrderId($db, $login_session, $orderResult);
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Your Shirt</title>

        <?php include 'common_html/common_css.php' ?>
    </head><!--/head-->

    <body>
        <header id="header"><!--header-->
            <?php include 'common_html/common_header.php' ?>
        </header><!--/header-->

        <section id="slider"><!--slider-->
            <?php include 'common_html/common_slider.php' ?>
        </section><!--/slider-->

        <section>
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <?php include 'common_html/common_sideBar.php' ?>
                    </div>
                    <div class="col-sm-9 padding-right"><!-- PS buat sini-->
                        <div class="features_items">
                            <h2 class="title text-center">My Cart</h2>
                            <div class="table-responsive cart_info">
                                <div class="form" style="float: right;">
                                    <div class="panel-heading">
                                        <table>
                                            <tr>
                                                <td><h4 class="title">Total: RM </h4></td>
                                                <td><h4><input type="text" id="totalPrice" name="total" style="border: 0;background-color: transparent; color: black" value="0.00" disabled></h4></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" style="text-align: right;">
                                                    <div><button class="btn btn-success" style="width: 100%;" onclick="checkOut();">Check Out</button></div>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="form">
                                    <form action="#" method="post">
                                        <?php
                                        if (isset($orderResult)) {
                                            if(mysqli_num_rows($orderResult) == 0){
                                                echo "<center><h2>NO CART.</h2></center>";
                                            }else{
                                                foreach ($orderResult as $row) {
                                                    echo "<table class=\"table table-condensed table-bordered table-hover\">"
                                                    . "<thead>"
                                                            . "<tr>"
                                                                . "<td colspan=\"7\" style=\"background-color: #f2f2f2;\"><input type=\"checkbox\" name=\"checkbox\" style=\"margin-right: 10px;\" id=\"".$row["order_id"]."\" onchange=\"checkBox('".$row["order_id"]."');\"> "
                                                                    . "<b>Placed On:</b> ".$row["create_date"]."  <b style=\"margin-left: 10px\">Order ID:</b> ".$row["order_id"]."</td>"
                                                                . "<td class=\"btn\" style=\"background-color: red; color: white;\" onclick=\"deleteOrder('".$row["order_id"]."');\">DELETE ORDER</td>"
                                                            . "</tr>"
                                                            . "<tr>"
                                                                . "<th style=\"width: 230px\"></th>"
                                                                . "<th>Product Name</th><th>Size</th>"
                                                                . "<th>Unit Price (RM)</th>"
                                                                . "<th>Quantity</th>"
                                                                . "<th>Status</th>"
                                                                . "<th>Total Price (RM)</th>"
                                                                . "<th>Action</th>"
                                                            ."</tr>"
                                                    . "</thead>"
                                                    . "<tbody>";
                                                    $cartClass->getCartDetail($db, $row["order_id"], $cartResult);
                                                    foreach($cartResult as $CartRow){
                                                        if(strlen((string)$CartRow["product_id"])  == 6){
                                                            $cartClass->getCustomizeDetail($db, $CartRow["product_id"], $customizeDetail);
                                                            $sleeve = strtolower($customizeDetail["sleeve"]);
                                                            $button = $customizeDetail["button"];
                                                            $collar = $customizeDetail["collar"];
                                                            $back = $customizeDetail["back"];
                                                            $color = strtolower($customizeDetail["color"]);
                                                            echo "<tr>"
                                                            . "<td onclick=\"viewImage('$sleeve', '$button', '$collar', '$back', '$color');\">"
                                                                    . "<div class=\"column\" style=\"float: left;\">"
                                                                            . "<img id=\"sleeve_img\" src=\"img/customize/$sleeve-$color.png\" style=\"max-width: 100px; position: absolute\" >"
                                                                            . "<img id=\"button_img\" src=\"img/customize/$button\" style=\"max-width: 100px; position: absolute\">"
                                                                            . "<img id=\"collar_img\" src=\"img/customize/$collar\" style=\"max-width: 100px; position: absolute\">"
                                                                            . "<img id=\"border\" src=\"img/customize/none.png\" style=\"max-width: 100px; position: relative\">"
                                                                        . "</div>"
                                                                        . "<div class=\"column\" style=\"float: right;\">"
                                                                            . "<img id=\"back_shirt\" src=\"img/customize/back-$sleeve-$color.png\" style=\"max-width: 100px; position: absolute\">"
                                                                            . "<img id=\"back_img\" src=\"img/customize/$back\" style=\"max-width: 100px; position: absolute\">"
                                                                            . "<img id=\"border\" src=\"img/customize/none.png\" style=\"max-width: 100px; position: relative\">"
                                                                    ."</div>"
                                                            . "</td>"
                                                            . "<td>Customized Shirt</td>"
                                                            . "<td>".$CartRow["size"]."</td>"
                                                            . "<td><input type=\"text\" value=\"" .number_format($CartRow["price"],2)."\" id=\"price_".$CartRow["orderDetailID"]."\" style=\"border: 0;background-color: transparent; color: black; max-width: 100px\" disabled></td>"
                                                            . "<td><input type=\"number\" value=\"1\" max=\"100\" min=\"1\" id=\"".$CartRow["orderDetailID"]."\" name=\"quantity\"  style=\"max-width: 75px\" onchange=\"productQty('".$CartRow["orderDetailID"]."', 'customize', '".$row["order_id"]."');\"></td>"
                                                            . "<td>Available</td>"
                                                            . "<td><input type=\"text\" id=\"subPrice_".$CartRow["orderDetailID"]."_".$row["order_id"]."\" name=\"subPrice\" style=\"border: 0;background-color: transparent; color: black; max-width: 100px\" disabled></td>"
                                                            . "<td><a href=\"#\" onclick=\"deleteCart('".$CartRow["orderDetailID"]."', '".$row["order_id"]."');\" style=\"color: red;\">DELETE</a></td>"
                                                            . "</tr>";
                                                        }else{
                                                            echo "<tr>"
                                                            . "<td><img src=\"img/ready_made/" . $CartRow["product_img"] . "\" style=\"width:100px;\"></td>"
                                                            . "<td><a href=\"#\" onclick=\"viewProduct('".$CartRow["RM_id"]."', '".$CartRow["product_name"]."');\">" . $CartRow["product_name"] . "</a></td>"
                                                            . "<td>".$CartRow["size"]."</td>"
                                                            . "<td><input type=\"text\" value=\"" .number_format($CartRow["product_price"],2)."\" id=\"price_".$CartRow["orderDetailID"]."\" style=\"border: 0;background-color: transparent; color: black; max-width: 100px\" disabled></td>"
                                                            . "<td><input type=\"number\" value=\"1\" max=\"".$CartRow["product_stock"]."\" min=\"1\" id=\"".$CartRow["orderDetailID"]."\" name=\"quantity\"  style=\"max-width: 75px\" onchange=\"productQty('".$CartRow["orderDetailID"]."', '".$CartRow["product_stock"]."', '".$row["order_id"]."');\"></td>"
                                                            . "<td>" . $CartRow["Status"] . "</td>"
                                                            . "<td><input type=\"text\" id=\"subPrice_".$CartRow["orderDetailID"]."_".$row["order_id"]."\" name=\"subPrice\" style=\"border: 0;background-color: transparent; color: black; max-width: 100px\" disabled></td>"
                                                            . "<td><a href=\"#\" onclick=\"deleteCart('".$CartRow["orderDetailID"]."', '".$row["order_id"]."');\" style=\"color: red;\">DELETE</a></td>"
                                                            . "</tr>";
                                                        }
                                                    }
                                                    $cartResult = NULL;
                                                    echo "</tbody>"
                                                    . "</table>";
                                                }
                                            }
                                        }
                                        ?>
                                    </form>
                                </div>
                            </div>
                        </div><!--features_items-->
                    </div>
                </div>
            </div>
        </section>

        <footer id="footer"><!--Footer-->
            <?php include 'common_html/common_footer.php' ?>
        </footer><!--/Footer-->



        <script src="js&css/jquery.js"></script>
        <script src="js&css/bootstrap.min.js"></script>
        <script src="js&css/jquery.scrollUp.min.js"></script>
        <script src="js&css/price-range.js"></script>
        <script src="js&css/jquery.prettyPhoto.js"></script>
        <script src="js&css/main.js"></script>
        <script src="js&css/Chart.js"></script>
        <script src="js&css/chart.js-php.js"></script>
        <script>            
            $(document).ready(function() {
                var subPrice = document.getElementsByName("subPrice");
                for(var i = 0; i < subPrice.length; i++){
                    var id = subPrice[i].id;
                    var orderDetailid = id.split("_")[1];
                    var orderid = id.split("_")[2];
                    
                    var price = document.getElementById("price_"+orderDetailid).value;
                    var qty = document.getElementById(orderDetailid).value;
                    var total = parseInt(price) * parseInt(qty);
                    document.getElementById("subPrice_"+orderDetailid+"_"+orderid).value = total.toFixed(2);
                }
            });
            
            function viewImage(sleeve, button, collar, back, color){
                window.open("viewCustomizedShirt.php?sleeve="+sleeve+"&button="+button+"&collar="+collar+"&back="+back+"&color="+color);
            };
            
            function viewProduct(rmid, productName){
                window.open("product_detail.php?rmid="+rmid+"&product="+productName);
            };
            
            function deleteOrder(orderid){
                if(confirm("Are you sure to delete this order?")){
                    var form_data = new FormData();
                    form_data.append("orderid", orderid);
                    form_data.append("action", "deleteOrder");
                    
                    $.ajax({
                        url: 'api/CartApi.php',
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data,
                        type: 'post',
                        success: function(response){
                            var obj = jQuery.parseJSON(response);
                            if(obj.ErrCode === '0'){
                                alert("Delete Successfully.");
                                location.href = "shopping_cart.php";
                            }else{
                                alert(obj.ErrCode+"-"+obj.ErrMsg);
                            }
                        }
                    });
                }
            };
            
            function deleteCart(orderDetailId, orderid){
                if(confirm("Are you sure to delete this product from your cart?")){
                    var form_data = new FormData();
                    form_data.append("orderDetailId", orderDetailId);
                    form_data.append("orderid", orderid);
                    form_data.append("action", "deleteCart");

                    $.ajax({
                        url: 'api/CartApi.php',
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data,
                        type: 'post',
                        success: function(response){
                            var obj = jQuery.parseJSON(response);
                            if(obj.ErrCode === '0'){
                                alert("Delete Successfully.");
                                location.href = "shopping_cart.php";
                            }else{
                                alert(obj.ErrCode+"-"+obj.ErrMsg);
                            }
                        }
                    });
                }
            };
            
            function productQty(id, max, orderid){
                var qty = document.getElementById(id).value;
                var basePrice = document.getElementById("price_"+id).value;
                if(max !== "customize"){
                    if(parseInt(qty) > parseInt(max)){
                        document.getElementById(id).value = max;
                    }
                }
                var total = parseInt(basePrice) * parseInt(document.getElementById(id).value);
                document.getElementById("subPrice_"+id+"_"+orderid).value = total.toFixed(2);
                
                checkBox();
            };
            
            function checkBox(){
                var totalPrice = 0;
                var checkbox = document.getElementsByName("checkbox");
                for(var i = 0; i < checkbox.length; i++){
                    if(checkbox[i].checked === true){
                        var orderid = checkbox[i].id;
                        var subPrice = document.getElementsByName("subPrice");
                        for(var j = 0; j < subPrice.length; j++){
                            var orderid2 = subPrice[j].id.split("_")[2];
                            if(orderid2 === orderid){
                                totalPrice = parseInt(totalPrice) + parseInt(subPrice[j].value);
                            }
                        }
                    }
                }
                document.getElementById("totalPrice").value = totalPrice.toFixed(2);
            };
            
            function checkOut(){
                if(confirm("Proceed to payment?")){
                    var finalResponse = 0;
                    var checkbox = document.getElementsByName("checkbox");       
                    var subPrice = document.getElementsByName("subPrice");
                    var newOrderDetail = [];
                    var x = 0;
                    for(var i = 0; i < checkbox.length; i++){
                        if(checkbox[i].checked === true){
                            var orderid = checkbox[i].id;
                            var y = 0;
                            newOrderDetail[x] = {"orderid": orderid};
                            for(var j = 0; j < subPrice.length; j++){
                                if(subPrice[j].id.split("_")[2] === orderid){
                                    var orderDetailId = subPrice[j].id.split("_")[1];
                                    var quantity = document.getElementById(orderDetailId).value;
                                    newOrderDetail[x][y] = {"orderDetailId" : orderDetailId, "quantity" : quantity};
                                    y++;
                                }
                            }
                            x++;
                        }
                    }
                    if(x===0){
                        alert("No Order is Checked.");
                    }else{
                        var jsonArray = JSON.stringify(newOrderDetail);
                        var form_data = new FormData();
                        form_data.append("order", jsonArray);
                        form_data.append("action", "checkOut");

                        $.ajax({
                            url: 'api/CartApi.php',
                            cache: false,
                            contentType: false,
                            processData: false,
                            data: form_data,
                            type: 'post',
                            success: function(response){
                                var obj = jQuery.parseJSON(response);
                                if(obj.ErrCode !== '0'){
                                    finalResponse++;
                                }
                            }
                        });

                        if(finalResponse === 0){
                            alert("Check Out Successfully.");
                            window.open("OrderSummary.php?order="+encodeURIComponent(jsonArray));
                        }else{
                            alert("Check Out Failed.");
                        }
                    }
                }
            };
        </script>
    </body>
</html>
