<?php
    include('../session.php');
    
    if(isset($_POST["action"])){
        $response = "";
        
        if($_POST["action"] === "receiveOrder"){
            $orderid = $_POST["orderid"];
            
            $update = mysqli_query($db, "UPDATE order_table SET delivery = 1 WHERE order_id = $orderid");
            if($update){
                $response = json_encode(array("ErrCode"=>'0', 'ErrMsg'=> "Update Successfully"));
            }else{
                $response = json_encode(array("ErrCode"=>'10001', 'ErrMsg'=> "Update Failed"));
            }
        }
        else if($_POST["action"] === "rating"){
            $rating = $_POST["rating"];
            $RM_id = $_POST["RM_id"];
            $userid = $login_session;
            
            $insert = mysqli_query($db, "INSERT INTO rating (user_id, RM_id, rating) "
                    . "VALUES ('$userid', '$RM_id', '$rating')");
            if($insert){
                $response = json_encode(array("ErrCode"=>'0', 'ErrMsg'=> "Update Successfully"));
            }else{
                $response = json_encode(array("ErrCode"=>'10001', 'ErrMsg'=> "Update Failed"));
            }
        }
        echo $response;
    }else{
        $response = json_encode(array("ErrCode"=>'20001', 'ErrMsg'=> 'Api Failed'));
        echo $response;
    }

