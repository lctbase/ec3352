<?php
    include('../session.php');
    
    if(isset($_POST["action"])){
        $response = "";      
        
        if($_POST["action"] === "customize"){
            $sleeve = $_POST["sleeve"];
            $button = $_POST["button"];
            $collar = $_POST["collar"];
            $back = $_POST["back"];
            $color = $_POST["color"];
            $size = $_POST["size"];
            $date = date("Y/m/d");
            $price = 0;
            
            // Get Customize Part ID
            $selectSleeve = mysqli_query($db, "SELECT * FROM sleeve WHERE sleeve_name = '".$sleeve."'");
            $selectButton = mysqli_query($db, "SELECT * FROM button WHERE button_img = '".$button."'");
            $selectCollar = mysqli_query($db, "SELECT * FROM collar WHERE collar_img = '".$collar."'");
            $selectBack = mysqli_query($db, "SELECT * FROM back WHERE back_img = '".$back."'");
            $selectColor = mysqli_query($db, "SELECT * FROM color WHERE color_name = '".$color."'");
            
            $customizeid = rand(100000,999999);
            $productid = rand(100000,999999);
            foreach ($selectSleeve as $row){ $sleeveid = $row["sleeve_id"]; $price = $price + (double)$row["sleeve_price"];}
            foreach ($selectButton as $row){ $buttonid = $row["button_id"]; $price = $price + (double)$row["button_price"];}
            foreach ($selectCollar as $row){ $collarid = $row["collar_id"]; $price = $price + (double)$row["collar_price"];}
            foreach ($selectBack as $row){ $backid = $row["back_id"]; $price = $price + (double)$row["back_price"];}
            foreach ($selectColor as $row){ $colorid = $row["color_id"]; $price = $price + (double)$row["color_price"];}

            // Insert into customize table
            $Insert = mysqli_query($db, "INSERT INTO customize (customize_id, product_id, sleeve_id, collar_id, back_id, button_id, color_id, price, size) "
                    . "VALUES ('$customizeid', '$productid', '$sleeveid', '$collarid', '$backid', '$buttonid', '$colorid', '$price', '$size')");
            if($Insert){
                // check order id is exist or not
                if(checkOrderID($db, $orderid, $login_session, $date)){
                    // insert order detail
                    if(addOrderDetail($db, $orderid, $productid)){
                        $response = json_encode(array("ErrCode"=>'0', 'ErrMsg'=> "Add Successfully"));
                    }else{
                        $response = json_encode(array("ErrCode"=>'10003', 'ErrMsg'=> "Add Failed"));
                    }
                }else{
                    $response = json_encode(array("ErrCode"=>'10002', 'ErrMsg'=> "Add Failed"));
                }
            }else{
                $response = json_encode(array("ErrCode"=>'10001', 'ErrMsg'=> "Add Failed"));
            }
        }
        else if($_POST["action"] === "ready_made"){
            $product = $_POST["productid"];
            $date = date("Y/m/d");
            
            // check order id is exist or not
            if(checkOrderID($db, $orderid, $login_session, $date)){
                // add order detail
                if(addOrderDetail($db, $orderid, $product)){
                     $response = json_encode(array("ErrCode"=>'0', 'ErrMsg'=> "Add Successfully"));
                }else{
                    $response = json_encode(array("ErrCode"=>'10002', 'ErrMsg'=> "Add Failed"));
                }
            }else{
                $response = json_encode(array("ErrCode"=>'10001', 'ErrMsg'=> "Add Failed"));
            }
        }
        else if($_POST["action"] === "deleteCart" ){
            $orderDetailId = $_POST["orderDetailId"];
            $orderid = $_POST["orderid"];
            $delete = mysqli_query($db, "DELETE FROM order_detail WHERE order_detail_id = '$orderDetailId'");
            if($delete){
                $response = json_encode(array("ErrCode"=>'0', 'ErrMsg'=> "Delete Successfully"));
                
                $checkOrder = mysqli_query($db, "SELECT * FROM order_detail WHERE order_id = $orderid");
                if($checkOrder){
                    if(mysqli_num_rows($checkOrder) == 0){
                        $deleteOrder = mysqli_query($db, "DELETE FROM order_table WHERE order_id = $orderid");
                        if($deleteOrder){
                            $response = json_encode(array("ErrCode"=>'0', 'ErrMsg'=> "Delete Successfully"));
                        }else{
                            $response = json_encode(array("ErrCode"=>'10003', 'ErrMsg'=> "Delete Failed"));
                        }
                    }
                }else{
                    $response = json_encode(array("ErrCode"=>'10002', 'ErrMsg'=> "Delete Failed"));
                }
            }else{
                $response = json_encode(array("ErrCode"=>'10001', 'ErrMsg'=> "Delete Failed"));
            }
        }
        else if($_POST["action"] === "deleteOrder"){
            $orderid = $_POST["orderid"];
            $deleteDetail = mysqli_query($db, "DELETE FROM order_detail WHERE order_id = '$orderid'");
            if($deleteDetail){
                $deleteOrder = mysqli_query($db, "DELETE FROM order_table WHERE order_id = $orderid");
                if($deleteOrder){
                    $response = json_encode(array("ErrCode"=>'0', 'ErrMsg'=> "Delete Successfully"));
                }else{
                    $response = json_encode(array("ErrCode"=>'10002', 'ErrMsg'=> "Delete Failed"));
                }
            }else{
                $response = json_encode(array("ErrCode"=>'10001', 'ErrMsg'=> "Delete Failed"));
            }
        }
        else if($_POST["action"] === "checkOut"){
            $orderDetail = json_decode($_POST["order"], true);
            $error = 0;
            $ordertgt = "";
            
            foreach($orderDetail as $row){
                $orderid = $row["orderid"];
                
                for($i = 0; $i < count($row)-1; $i++){
                    $detailId = $row[$i]["orderDetailId"];
                    $quantity = $row[$i]["quantity"];
                    
                    $updateDetail = mysqli_query($db, "UPDATE order_detail SET quantity = $quantity WHERE order_detail_id = $detailId");
                    if($updateDetail){
                        $selectProduct = mysqli_query($db, "SELECT * FROM order_detail WHERE order_detail_id = $detailId");
                        foreach($selectProduct as $product){$productid = $product["product_id"];}
                        if(strlen((string)$productid) != 6){
                            $selectRM = mysqli_query($db, "SELECT * FROM ready_made WHERE product_id = $productid");
                            foreach($selectRM as $getRM){ $rm_id = $getRM["RM_id"];}
                            if($ordertgt == ""){
                                $ordertgt = $rm_id;
                            }else{
                                $ordertgt = $ordertgt.",".$rm_id;
                            }
                            $updateProduct = mysqli_query($db, "UPDATE ready_made SET product_stock = product_stock - $quantity WHERE product_id = $productid");
                            if(!$updateProduct){
                                $response = json_encode(array("ErrCode"=>'10002', 'ErrMsg'=> "Update Failed"));
                            }
                        }
                    }else{
                        $error++;
                    }
                    
                    if($error == 0){
                        $updateOrder = mysqli_query($db, "UPDATE order_table SET status = 1, order_tgt = '$ordertgt' WHERE order_id = $orderid");
                        if($updateOrder){
                            $response = json_encode(array("ErrCode"=>'0', 'ErrMsg'=> "Update Succcessfully"));
                        }else{
                            $response = json_encode(array("ErrCode"=>'10001', 'ErrMsg'=> "Update Failed"));
                        }
                    }
                }
            }
        }
        echo $response;
    }else{
        $response = json_encode(array("ErrCode"=>'20001', 'ErrMsg'=> 'Api Failed'));
        echo $response;
    }
    


    function checkOrderID($db, $orderid, $login_session, $date){
        $checkorder = mysqli_query($db, "SELECT * FROM order_table WHERE order_id = ".$orderid);
        if(mysqli_num_rows($checkorder)==0){
            // add new order id
            $insertOrder = mysqli_query($db, "INSERT INTO order_table (order_id, user_id, create_date, status, delivery) "
                    . "VALUES ('$orderid', '$login_session', '$date', '0', '0')");
            if($insertOrder){
                return true;
            }else{
                return false;
            }
        }else{
            return true;
        }
    }
    
    function addOrderDetail($db, $orderid, $productid){
        $insertOrderDetail = mysqli_query($db, "INSERT INTO order_detail (order_id, product_id, quantity) "
            . "VALUES ('$orderid', '$productid', '1')");
        if($insertOrderDetail){
            return true;
        }else{
            return false;
        }
    }
