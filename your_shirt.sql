-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 25, 2019 at 07:46 AM
-- Server version: 10.1.33-MariaDB
-- PHP Version: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `your_shirt`
--

-- --------------------------------------------------------

--
-- Table structure for table `back`
--

CREATE TABLE `back` (
  `back_id` int(16) NOT NULL,
  `back_name` text NOT NULL,
  `back_price` double NOT NULL,
  `back_img` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `back`
--

INSERT INTO `back` (`back_id`, `back_name`, `back_price`, `back_img`) VALUES
(1, 'Center', 40, 'back-center.png'),
(2, 'Side', 40, 'back-side.png'),
(3, 'No Back', 0, 'none.png');

-- --------------------------------------------------------

--
-- Table structure for table `button`
--

CREATE TABLE `button` (
  `button_id` int(16) NOT NULL,
  `button_name` text NOT NULL,
  `button_price` double NOT NULL,
  `button_img` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `button`
--

INSERT INTO `button` (`button_id`, `button_name`, `button_price`, `button_img`) VALUES
(1, 'Black', 10, 'button-black.png'),
(2, 'Grey', 10, 'button-grey.png'),
(3, 'Red', 10, 'button-red.png'),
(4, 'White', 10, 'button-white.png');

-- --------------------------------------------------------

--
-- Table structure for table `collar`
--

CREATE TABLE `collar` (
  `collar_id` int(16) NOT NULL,
  `collar_name` text NOT NULL,
  `collar_price` double NOT NULL,
  `collar_img` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `collar`
--

INSERT INTO `collar` (`collar_id`, `collar_name`, `collar_price`, `collar_img`) VALUES
(1, 'Button Down Black', 10, 'collar-black.png'),
(2, 'Button Down Grey', 10, 'collar-grey.png'),
(3, 'Button Down Red', 10, 'collar-red.png'),
(4, 'Button Down White', 10, 'collar-white.png'),
(5, 'Basic Collar', 0, 'none.png');

-- --------------------------------------------------------

--
-- Table structure for table `color`
--

CREATE TABLE `color` (
  `color_id` int(16) NOT NULL,
  `color_name` text NOT NULL,
  `color_price` double NOT NULL,
  `color_img` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `color`
--

INSERT INTO `color` (`color_id`, `color_name`, `color_price`, `color_img`) VALUES
(1, 'Black', 0, 'long-black.png'),
(2, 'Blue', 20, 'long-blue.png'),
(3, 'Grey', 0, 'long-grey.png'),
(4, 'White', 0, 'long-white.png');

-- --------------------------------------------------------

--
-- Table structure for table `customize`
--

CREATE TABLE `customize` (
  `customize_id` int(16) NOT NULL,
  `product_id` int(16) NOT NULL,
  `sleeve_id` int(16) NOT NULL,
  `collar_id` int(16) NOT NULL,
  `fabric_id` int(16) NOT NULL,
  `back_id` int(16) NOT NULL,
  `button_id` int(16) NOT NULL,
  `color_id` int(16) NOT NULL,
  `price` double NOT NULL,
  `preview_img` varchar(10) NOT NULL,
  `size` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `order_detail`
--

CREATE TABLE `order_detail` (
  `order_detail_id` int(16) NOT NULL,
  `order_id` int(16) NOT NULL,
  `product_id` int(16) NOT NULL,
  `quantity` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `order_table`
--

CREATE TABLE `order_table` (
  `order_id` int(16) NOT NULL,
  `user_id` int(16) NOT NULL,
  `create_date` date NOT NULL,
  `status` text NOT NULL,
  `delivery` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rating`
--

CREATE TABLE `rating` (
  `rate_id` int(16) NOT NULL,
  `RM_id` int(16) NOT NULL,
  `rating` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rating`
--

INSERT INTO `rating` (`rate_id`, `RM_id`, `rating`) VALUES
(1, 1, '4'),
(2, 1, '5');

-- --------------------------------------------------------

--
-- Table structure for table `ready_made`
--

CREATE TABLE `ready_made` (
  `RM_id` int(16) NOT NULL,
  `product_id` int(16) NOT NULL,
  `product_name` text NOT NULL,
  `product_price` double NOT NULL,
  `product_stock` int(100) NOT NULL,
  `product_img` varchar(100) NOT NULL,
  `size` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ready_made`
--

INSERT INTO `ready_made` (`RM_id`, `product_id`, `product_name`, `product_price`, `product_stock`, `product_img`, `size`) VALUES
(1, 1, 'Autumn Style for Man', 130, 20, 'Autumn_Style_Man.jpg', 'M'),
(1, 2, 'Autumn Style for Man', 130, 15, 'Autumn_Style_Man.jpg', 'L'),
(2, 3, 'Classic Blue for Man', 110, 10, 'Class_Blue_Man.jpg', 'S'),
(2, 4, 'Classic Blue for Man', 110, 18, 'Class_Blue_Man.jpg', 'M'),
(2, 5, 'Classic Blue for Man', 110, 9, 'Class_Blue_Man.jpg', 'L'),
(3, 6, 'Testing', 200, 9999, 'Autumn_Style_Man.jpg', 'S'),
(3, 7, 'Testing', 200, 1, 'Autumn_Style_Man.jpg', 'M');

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `role_id` int(1) NOT NULL,
  `role_name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`role_id`, `role_name`) VALUES
(0, 'Normal'),
(1, 'admin'),
(2, 'user');

-- --------------------------------------------------------

--
-- Table structure for table `sleeve`
--

CREATE TABLE `sleeve` (
  `sleeve_id` int(16) NOT NULL,
  `sleeve_name` text NOT NULL,
  `sleeve_price` double NOT NULL,
  `sleeve_img` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sleeve`
--

INSERT INTO `sleeve` (`sleeve_id`, `sleeve_name`, `sleeve_price`, `sleeve_img`) VALUES
(1, 'Long', 50, ''),
(2, 'Short', 30, '');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(16) NOT NULL,
  `user_password` varchar(16) NOT NULL,
  `user_fname` text NOT NULL,
  `user_lname` text NOT NULL,
  `user_email` varchar(30) NOT NULL,
  `user_address` varchar(100) NOT NULL,
  `role_id` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `user_password`, `user_fname`, `user_lname`, `user_email`, `user_address`, `role_id`) VALUES
(14723, 'abcd1234', 'Jian Wei', 'Long', 'wei972008@hotmail.com', 'Taman Titi', 1),
(919552252, 'asdf', 'Jun Hao', 'Lee', 'asdf@gmail.com', 'asdf', 2),
(1193171035, 'tom2u1004', 'PS', 'Tong', 'soon970410@live.com', 'BP', 2),
(1867924730, 'abcd1234', 'Jian Wei', 'Long', '12345@gmail.com', 'nilai', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `back`
--
ALTER TABLE `back`
  ADD PRIMARY KEY (`back_id`);

--
-- Indexes for table `button`
--
ALTER TABLE `button`
  ADD PRIMARY KEY (`button_id`);

--
-- Indexes for table `collar`
--
ALTER TABLE `collar`
  ADD PRIMARY KEY (`collar_id`);

--
-- Indexes for table `color`
--
ALTER TABLE `color`
  ADD PRIMARY KEY (`color_id`);

--
-- Indexes for table `customize`
--
ALTER TABLE `customize`
  ADD PRIMARY KEY (`customize_id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `sleeve_id` (`sleeve_id`),
  ADD KEY `back_id` (`back_id`),
  ADD KEY `button_id` (`button_id`),
  ADD KEY `color_id` (`color_id`),
  ADD KEY `collar_id` (`collar_id`),
  ADD KEY `sleeve_id_2` (`sleeve_id`);

--
-- Indexes for table `order_detail`
--
ALTER TABLE `order_detail`
  ADD PRIMARY KEY (`order_detail_id`),
  ADD KEY `order_id` (`order_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `order_table`
--
ALTER TABLE `order_table`
  ADD PRIMARY KEY (`order_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `rating`
--
ALTER TABLE `rating`
  ADD PRIMARY KEY (`rate_id`),
  ADD KEY `RM_id` (`RM_id`);

--
-- Indexes for table `ready_made`
--
ALTER TABLE `ready_made`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`role_id`);

--
-- Indexes for table `sleeve`
--
ALTER TABLE `sleeve`
  ADD PRIMARY KEY (`sleeve_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`),
  ADD KEY `role_id` (`role_id`),
  ADD KEY `role_id_2` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customize`
--
ALTER TABLE `customize`
  MODIFY `customize_id` int(16) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ready_made`
--
ALTER TABLE `ready_made`
  MODIFY `product_id` int(16) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `customize`
--
ALTER TABLE `customize`
  ADD CONSTRAINT `customize_ibfk_1` FOREIGN KEY (`collar_id`) REFERENCES `collar` (`collar_id`),
  ADD CONSTRAINT `customize_ibfk_2` FOREIGN KEY (`color_id`) REFERENCES `color` (`color_id`),
  ADD CONSTRAINT `customize_ibfk_3` FOREIGN KEY (`sleeve_id`) REFERENCES `sleeve` (`sleeve_id`),
  ADD CONSTRAINT `customize_ibfk_4` FOREIGN KEY (`button_id`) REFERENCES `button` (`button_id`),
  ADD CONSTRAINT `customize_ibfk_5` FOREIGN KEY (`back_id`) REFERENCES `back` (`back_id`);

--
-- Constraints for table `order_detail`
--
ALTER TABLE `order_detail`
  ADD CONSTRAINT `order_detail_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `order_table` (`order_id`);

--
-- Constraints for table `order_table`
--
ALTER TABLE `order_table`
  ADD CONSTRAINT `order_table_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`);

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `role` (`role_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
