<?php
include('session.php');

if(!isset($login_session)){
    header('Location: login_user.php');
}
include_once 'backEnd/cart_Class.php';
include_once 'backEnd/user_Class.php';
$cartClass = new Cart_Class();
$userClass = new User_Class();
$userClass->getCurrentOrderId($db, $login_session, $currentOrder);
$userClass->getOrderHistory($db, $login_session, $orderHistory);
$userClass->getUserDetail($db, $login_session, $userDetail);
foreach($userDetail as $row){
    $UID = $row["user_id"];
    $fName = $row["user_fname"];
    $lName = $row["user_lname"];
    $email = $row["user_email"];
    $address = $row["user_address"];
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Your Shirt</title>

        <?php include 'common_html/common_css.php' ?>
    </head><!--/head-->

    <body>
        <header id="header"><!--header-->
            <?php include 'common_html/common_header.php' ?>
        </header><!--/header-->

        <section id="slider"><!--slider-->
            <?php include 'common_html/common_slider.php' ?>
        </section><!--/slider-->

        <section>
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <?php include 'common_html/common_sideBar.php' ?>
                    </div>
                    <div class="col-sm-9 padding-right"><!-- PS buat sini-->
                        <div class="features_items">
                            <h2 class="title text-center">MY PROFILE</h2>
                            <div class="table-responsive cart_info">
                                <div class="form">
                                    <div class="panel-heading">
                                        <div class="productinfo text-center" style="float:left;">
                                            <img src="img/user-img.jpg" alt="" style="max-width: 125px;"/>
                                        </div>
                                        <table style="font-size: 20px;margin: auto; width: 60%">
                                            <tr>
                                                <th style="width: 30%">User ID</th>
                                                <td><?=$UID?></td>
                                            </tr>
                                            <tr>
                                                <th>User Name</th>
                                                <td><?=$lName?>, <?=$fName?></td>
                                            </tr>
                                            <tr>
                                                <th>Email</th>
                                                <td><?=$email?></td>
                                            </tr>
                                            <tr>
                                                <th>Address</th>
                                                <td><?=$address?></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <hr>
                                <div class="form">
                                    <form action="#" method="post">
                                        <h4><center>Current Order</center></h4>
                                        <?php
                                            $totalPrice = 0;
                                            if (isset($currentOrder)) {
                                                if(mysqli_num_rows($currentOrder) == 0){
                                                    echo "<center><h2>NO ORDER.</h2></center>";
                                                }else{
                                                    foreach ($currentOrder as $row) {
                                                        echo "<table class=\"table table-condensed table-bordered table-hover\">"
                                                        . "<thead>"
                                                                . "<tr>"
                                                                    . "<td colspan=\"3\" style=\"background-color: #f2f2f2;\"><b style=\"margin-left: 10px\">Order ID:</b> ".$row["order_id"]."</td>"
                                                                    . "<td style=\"background-color: #808080; color: white;\" onclick=\"showDetail('".$row["order_id"]."');\">Show Details</td>"
                                                                    . "<td class=\"btn\" style=\"background-color: #33A532; color: white; width: 100%\" onclick=\"receivedOrder('".$row["order_id"]."');\">RECEIVED</td>"
                                                                . "</tr>"
                                                        . "</thead>"
                                                        . "<tbody id=\"".$row["order_id"]."\" hidden>"
                                                                . "<tr>"
                                                                    . "<th style=\"max-width: 250px\"></th>"
                                                                    . "<th>Product Name</th>"
                                                                    . "<th>Size</th>"
                                                                    . "<th>Quantity</th>"
                                                                    . "<th>Price (RM)</th>"
                                                                ."</tr>";
                                                        $cartClass->getCartDetail($db, $row["order_id"], $cartResult);
                                                        foreach($cartResult as $CartRow){
                                                            if(strlen((string)$CartRow["product_id"])  == 6){
                                                                $cartClass->getCustomizeDetail($db, $CartRow["product_id"], $customizeDetail);
                                                                $sleeve = strtolower($customizeDetail["sleeve"]);
                                                                $button = $customizeDetail["button"];
                                                                $collar = $customizeDetail["collar"];
                                                                $back = $customizeDetail["back"];
                                                                $color = strtolower($customizeDetail["color"]);
                                                                $totalPrice = $totalPrice + ($CartRow["price"] * $CartRow["orderQuantity"]);
                                                                echo "<tr>"
                                                                . "<td onclick=\"viewImage('$sleeve', '$button', '$collar', '$back', '$color');\">"
                                                                        . "<div class=\"column\" style=\"float: left;\">"
                                                                                . "<img id=\"sleeve_img\" src=\"img/customize/$sleeve-$color.png\" style=\"max-width:100px; position: absolute\" >"
                                                                                . "<img id=\"button_img\" src=\"img/customize/$button\" style=\"max-width:100px; position: absolute\">"
                                                                                . "<img id=\"collar_img\" src=\"img/customize/$collar\" style=\"max-width:100px; position: absolute\">"
                                                                                . "<img id=\"border\" src=\"img/customize/none.png\" style=\"max-width:100px; position: relative\">"
                                                                            . "</div>"
                                                                            . "<div class=\"column\" style=\"float: right;\">"
                                                                                . "<img id=\"back_shirt\" src=\"img/customize/back-$sleeve-$color.png\" style=\"max-width:100px; position: absolute\">"
                                                                                . "<img id=\"back_img\" src=\"img/customize/$back\" style=\"max-width:100px; position: absolute\">"
                                                                                . "<img id=\"border\" src=\"img/customize/none.png\" style=\"max-width:100px; position: relative\">"
                                                                        ."</div>"
                                                                . "</td>"
                                                                . "<td>Customized Shirt</td>"
                                                                . "<td>".$CartRow["size"]."</td>"
                                                                . "<td>".$CartRow["orderQuantity"]."</td>"
                                                                . "<td>".number_format($CartRow["price"] * $CartRow["orderQuantity"],2)."</td>"
                                                                . "</tr>";
                                                            }else{
                                                                $totalPrice = $totalPrice + ($CartRow["product_price"] * $CartRow["orderQuantity"]);
                                                                echo "<tr>"
                                                                . "<td><img src=\"img/ready_made/" . $CartRow["product_img"] . "\" style=\"width:100px;\"></td>"
                                                                . "<td><a href=\"#\" onclick=\"viewProduct('".$CartRow["RM_id"]."', '".$CartRow["product_name"]."');\">" . $CartRow["product_name"] . "</a></td>"
                                                                . "<td>".$CartRow["size"]."</td>"
                                                                . "<td>".$CartRow["orderQuantity"]."</td>"
                                                                . "<td>".number_format($CartRow["product_price"] * $CartRow["orderQuantity"],2)."</td>"
                                                                . "</tr>";
                                                            }
                                                        }
                                                        echo "<tr>"
                                                            . "<td colspan=5 style=\"text-align: right;\"><h4>Total: RM ".number_format($totalPrice, 2)."</td>"
                                                            . "</tr>";
                                                        $cartResult = NULL;
                                                        $totalPrice = 0;
                                                        echo "</tbody>"
                                                        . "</table>";
                                                    }
                                                }
                                            }
                                        ?>
                                    </form>
                                </div>
                                <hr>
                                <div class="form">
                                    <form action="#" method="post">
                                        <h4><center>Order History</center></h4>
                                        <?php
                                            $totalPrice = 0;
                                            if (isset($orderHistory)) {
                                                if(mysqli_num_rows($orderHistory) == 0){
                                                    echo "<center><h2>NO ORDER.</h2></center>";
                                                }else{
                                                    foreach ($orderHistory as $row) {
                                                        echo "<table class=\"table table-condensed table-bordered table-hover\">"
                                                        . "<thead>"
                                                                . "<tr>"
                                                                    . "<td colspan=\"4\" style=\"background-color: #f2f2f2;\"><b style=\"margin-left: 10px\">Order ID:</b> ".$row["order_id"]."</td>"
                                                                    . "<td style=\"background-color: #808080; color: white;\" onclick=\"showDetail('".$row["order_id"]."');\">Show Details</td>"
                                                                    . "<td class=\"btn\" style=\"background-color: red; color: white; width: 100%\" onclick=\"deleteHistory('".$row["order_id"]."');\">DELETE</td>"
                                                                . "</tr>"
                                                        . "</thead>"
                                                        . "<tbody id=\"".$row["order_id"]."\" hidden>"
                                                                . "<tr>"
                                                                    . "<th style=\"max-width: 250px\"></th>"
                                                                    . "<th>Product Name</th>"
                                                                    . "<th>Size</th>"
                                                                    . "<th>Quantity</th>"
                                                                    . "<th>Price (RM)</th>"
                                                                    . "<th>Your Rating</th>"
                                                                ."</tr>";
                                                        $cartClass->getCartDetail($db, $row["order_id"], $cartResult);
                                                        foreach($cartResult as $CartRow){
                                                            if(strlen((string)$CartRow["product_id"])  == 6){
                                                                $cartClass->getCustomizeDetail($db, $CartRow["product_id"], $customizeDetail);
                                                                $sleeve = strtolower($customizeDetail["sleeve"]);
                                                                $button = $customizeDetail["button"];
                                                                $collar = $customizeDetail["collar"];
                                                                $back = $customizeDetail["back"];
                                                                $color = strtolower($customizeDetail["color"]);
                                                                $totalPrice = $totalPrice + ($CartRow["price"] * $CartRow["orderQuantity"]);
                                                                echo "<tr>"
                                                                . "<td onclick=\"viewImage('$sleeve', '$button', '$collar', '$back', '$color');\">"
                                                                        . "<div class=\"column\" style=\"float: left;\">"
                                                                                . "<img id=\"sleeve_img\" src=\"img/customize/$sleeve-$color.png\" style=\"max-width:100px; position: absolute\" >"
                                                                                . "<img id=\"button_img\" src=\"img/customize/$button\" style=\"max-width:100px; position: absolute\">"
                                                                                . "<img id=\"collar_img\" src=\"img/customize/$collar\" style=\"max-width:100px; position: absolute\">"
                                                                                . "<img id=\"border\" src=\"img/customize/none.png\" style=\"max-width:100px; position: relative\">"
                                                                            . "</div>"
                                                                            . "<div class=\"column\" style=\"float: right;\">"
                                                                                . "<img id=\"back_shirt\" src=\"img/customize/back-$sleeve-$color.png\" style=\"max-width:100px; position: absolute\">"
                                                                                . "<img id=\"back_img\" src=\"img/customize/$back\" style=\"max-width:100px; position: absolute\">"
                                                                                . "<img id=\"border\" src=\"img/customize/none.png\" style=\"max-width:100px; position: relative\">"
                                                                        ."</div>"
                                                                . "</td>"
                                                                . "<td>Customized Shirt</td>"
                                                                . "<td>".$CartRow["size"]."</td>"
                                                                . "<td>".$CartRow["orderQuantity"]."</td>"
                                                                . "<td>".number_format($CartRow["price"] * $CartRow["orderQuantity"],2)."</td>"
                                                                . "<td>-</td>"
                                                                . "</tr>";
                                                            }else{
                                                                $totalPrice = $totalPrice + ($CartRow["product_price"] * $CartRow["orderQuantity"]);
                                                                $userClass->getUserRating($db, $login_session, $CartRow["RM_id"], $userRate);
                                                                echo "<tr>"
                                                                . "<td><img src=\"img/ready_made/" . $CartRow["product_img"] . "\" style=\"width:100px;\"></td>"
                                                                . "<td><a href=\"#\" onclick=\"viewProduct('".$CartRow["RM_id"]."', '".$CartRow["product_name"]."');\">" . $CartRow["product_name"] . "</a></td>"
                                                                . "<td>".$CartRow["size"]."</td>"
                                                                . "<td>".$CartRow["orderQuantity"]."</td>"
                                                                . "<td>".number_format($CartRow["product_price"] * $CartRow["orderQuantity"],2)."</td>"
                                                                . "<td>";
                                                                if($userRate == 0){
                                                                    echo "<select id=\"".$CartRow["orderDetailID"]."\" onchange=\"rating('".$CartRow["orderDetailID"]."', '".$CartRow["RM_id"]."');\">"
                                                                        . "<option value='0'>Please Rate</option>"
                                                                        . "<option value='5'>5</option>"
                                                                        . "<option value='4'>4</option>"
                                                                        . "<option value='3'>3</option>"
                                                                        . "<option value='2'>2</option>"
                                                                        . "<option value='1'>1</option>"
                                                                        ."</select>";
                                                                }else{
                                                                    echo $userRate;
                                                                }
                                                                echo "</td>"
                                                                . "</tr>";
                                                            }
                                                        }
                                                        echo "<tr>"
                                                            . "<td colspan=6 style=\"text-align: right;\"><h4>Total: RM ".number_format($totalPrice, 2)."</td>"
                                                            . "</tr>";
                                                        $userRate = NULL;
                                                        $cartResult = NULL;
                                                        $totalPrice = 0;
                                                        echo "</tbody>"
                                                        . "</table>";
                                                    }
                                                }
                                            }
                                        ?>
                                    </form>
                                </div>
                            </div>
                        </div><!--features_items-->
                    </div>
                </div>
            </div>
        </section>

        <footer id="footer"><!--Footer-->
            <?php include 'common_html/common_footer.php' ?>
        </footer><!--/Footer-->



        <script src="js&css/jquery.js"></script>
        <script src="js&css/bootstrap.min.js"></script>
        <script src="js&css/jquery.scrollUp.min.js"></script>
        <script src="js&css/price-range.js"></script>
        <script src="js&css/jquery.prettyPhoto.js"></script>
        <script src="js&css/main.js"></script>
        <script src="js&css/Chart.js"></script>
        <script src="js&css/chart.js-php.js"></script>
        <script>            
            function viewImage(sleeve, button, collar, back, color){
                window.open("viewCustomizedShirt.php?sleeve="+sleeve+"&button="+button+"&collar="+collar+"&back="+back+"&color="+color);
            };
            
            function viewProduct(rmid, productName){
                window.open("product_detail.php?rmid="+rmid+"&product="+productName);
            };
            
            function showDetail(id){
                if($('#'+id).is(":hidden")){
                    $('#'+id).removeAttr('hidden');
                }else{
                    $('#'+id).attr("hidden",true);
                }
            };
            
            function receivedOrder(orderid){
                if(confirm("Are you sure that you had received the products?")){
                    var form_data = new FormData();
                    form_data.append("orderid", orderid);
                    form_data.append("action", "receiveOrder");
                    
                    $.ajax({
                        url: 'api/UserApi.php',
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data,
                        type: 'post',
                        success: function(response){
                            var obj = jQuery.parseJSON(response);
                            if(obj.ErrCode === '0'){
                                alert("Thank You!");
                                location.href = "user_profile.php";
                            }else{
                                alert(obj.ErrCode+"-"+obj.ErrMsg);
                            }
                        }
                    });
                }
            };
            
            function deleteHistory(orderid){
                if(confirm("Are you sure to delete this order from your history?")){
                    var form_data = new FormData();
                    form_data.append("orderid", orderid);
                    form_data.append("action", "deleteOrder");
                    
                    $.ajax({
                        url: 'api/CartApi.php',
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data,
                        type: 'post',
                        success: function(response){
                            var obj = jQuery.parseJSON(response);
                            if(obj.ErrCode === '0'){
                                alert("Deleted!");
                                location.href = "user_profile.php";
                            }else{
                                alert(obj.ErrCode+"-"+obj.ErrMsg);
                            }
                        }
                    });
                }
            }
            
            function rating(orderDetailId, RM_id){
                var rating = document.getElementById(orderDetailId).value;
                var form_data = new FormData();
                form_data.append("RM_id", RM_id);
                form_data.append("rating", rating);
                form_data.append("action", "rating");
                
                $.ajax({
                    url: 'api/UserApi.php',
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,
                    type: 'post',
                    success: function(response){
                        var obj = jQuery.parseJSON(response);
                    }
                });
            };
        </script>
    </body>
    
</html>
