<?php
include('session.php');
if (!isset($_GET["order"])) {
    header('Location: index.php');
} else {
    include_once 'backEnd/cart_Class.php';
    $orderDetail = json_decode($_GET["order"], true);
    $cartClass = new Cart_Class();
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Your Shirt</title>

        <?php include 'common_html/common_css.php' ?>
    </head><!--/head-->

    <body>
        <section style="margin-top: 20px;">
            <div class="container">
                <div class="row">
                    <div class="features_items">
                        <h2 class="title text-center">Order Summary</h2>
                        <div class="table-responsive cart_info">
                            <div class="form">
                                <form action="user_profile.php" method="post">
                                    <table class="table table-condensed table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>Product Image</th>
                                                <th>Product Name</th>
                                                <th>Size</th>
                                                <th>Unit Price (RM)</th>
                                                <th>Quantity</th>
                                                <th>SubTotal (RM)</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                $totalPrice = 0;
                                                foreach($orderDetail as $row){
                                                    $orderid = $row["orderid"];
                                                    $cartClass->getCartDetail($db, $orderid, $cartResult);
                                                    foreach($cartResult as $CartRow){
                                                        if(strlen((string)$CartRow["product_id"])  == 6){
                                                            $cartClass->getCustomizeDetail($db, $CartRow["product_id"], $customizeDetail);
                                                            $sleeve = strtolower($customizeDetail["sleeve"]);
                                                            $button = $customizeDetail["button"];
                                                            $collar = $customizeDetail["collar"];
                                                            $back = $customizeDetail["back"];
                                                            $color = strtolower($customizeDetail["color"]);
                                                            $totalPrice = $totalPrice + ($CartRow["price"] * $CartRow["orderQuantity"]);
                                                            echo "<tr>"
                                                            . "<td onclick=\"viewImage('$sleeve', '$button', '$collar', '$back', '$color');\">"
                                                                    . "<div class=\"column\" style=\"float: left;\">"
                                                                            . "<img id=\"sleeve_img\" src=\"img/customize/$sleeve-$color.png\" style=\"max-width: 100px; position: absolute\" >"
                                                                            . "<img id=\"button_img\" src=\"img/customize/$button\" style=\"max-width: 100px; position: absolute\">"
                                                                            . "<img id=\"collar_img\" src=\"img/customize/$collar\" style=\"max-width: 100px; position: absolute\">"
                                                                            . "<img id=\"border\" src=\"img/customize/none.png\" style=\"max-width: 100px; position: relative\">"
                                                                        . "</div>"
                                                                        . "<div class=\"column\" style=\"float: right;\">"
                                                                            . "<img id=\"back_shirt\" src=\"img/customize/back-$sleeve-$color.png\" style=\"max-width: 100px; position: absolute\">"
                                                                            . "<img id=\"back_img\" src=\"img/customize/$back\" style=\"max-width: 100px; position: absolute\">"
                                                                            . "<img id=\"border\" src=\"img/customize/none.png\" style=\"max-width: 100px; position: relative\">"
                                                                    ."</div>"
                                                            . "</td>"
                                                            . "<td>Customized Shirt</td>"
                                                            . "<td>".$CartRow["size"]."</td>"
                                                            . "<td><input type=\"text\" value=\"" .number_format($CartRow["price"],2)."\" id=\"price_".$CartRow["orderDetailID"]."\" style=\"border: 0;background-color: transparent; color: black; max-width: 100px\" disabled></td>"
                                                            . "<td>".$CartRow["orderQuantity"]."</td>"
                                                            . "<td>".number_format($CartRow["price"] * $CartRow["orderQuantity"],2)."</td>"
                                                            . "</tr>";
                                                        }else{
                                                            $totalPrice = $totalPrice + ($CartRow["product_price"] * $CartRow["orderQuantity"]);
                                                            echo "<tr>"
                                                            . "<td><img src=\"img/ready_made/" . $CartRow["product_img"] . "\" style=\"width:100px;\"></td>"
                                                            . "<td><a href=\"#\" onclick=\"viewProduct('".$CartRow["RM_id"]."', '".$CartRow["product_name"]."');\">" . $CartRow["product_name"] . "</a></td>"
                                                            . "<td>".$CartRow["size"]."</td>"
                                                            . "<td><input type=\"text\" value=\"" .number_format($CartRow["product_price"],2)."\" id=\"price_".$CartRow["orderDetailID"]."\" style=\"border: 0;background-color: transparent; color: black; max-width: 100px\" disabled></td>"
                                                            . "<td>".$CartRow["orderQuantity"]."</td>"
                                                            . "<td>".number_format($CartRow["product_price"] * $CartRow["orderQuantity"],2)."</td>"
                                                            . "</tr>";
                                                        }
                                                    }
                                                    $cartResult = NULL;
                                                }
                                            ?>
                                        </tbody>
                                    </table>
                                    <hr>
                                    <div class="form" style="float: right;">
                                        <div class="panel-heading">
                                            <table>
                                                <tr>
                                                    <td><h3 class="title">Total: RM <?=number_format($totalPrice, 2)?></h3></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2" style="text-align: right;">
                                                        <div><button class="btn btn-success" style="width: 100%;" type="submit">Done</button></div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div><!--features_items-->
                </div>
            </div>
        </section>
        
        <script src="js&css/jquery.js"></script>
        <script src="js&css/bootstrap.min.js"></script>
        <script src="js&css/jquery.scrollUp.min.js"></script>
        <script src="js&css/price-range.js"></script>
        <script src="js&css/jquery.prettyPhoto.js"></script>
        <script src="js&css/main.js"></script>
        <script src="js&css/Chart.js"></script>
        <script src="js&css/chart.js-php.js"></script>
        <script>
            function viewImage(sleeve, button, collar, back, color){
                window.open("viewCustomizedShirt.php?sleeve="+sleeve+"&button="+button+"&collar="+collar+"&back="+back+"&color="+color);
            };
            
            function viewProduct(rmid, productName){
                window.open("product_detail.php?rmid="+rmid+"&product="+productName);
            };
        </script>
    </body>
</html>


