<?php
//include("config.php");
include('session.php');

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $myuserid = mysqli_real_escape_string($db, $_POST['userid']);
    $mypassword = mysqli_real_escape_string($db, $_POST['userpassword']);

    $sql = "SELECT user_id, role_id FROM user WHERE user_email='$myuserid' and user_password='$mypassword'";
    $result = mysqli_query($db, $sql);


    $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
    $active = $row['active'];

    $role_id = $row["role_id"];
    $count = mysqli_num_rows($result);

    if ($count == 1) {
        if ($role_id == 1) {
            $_SESSION['myuserid'] = $myuserid;
            $_SESSION['login_admin'] = $myuserid;

            header("Location: admin_index.php");
        } else if ($role_id == 2) {
            $_SESSION['myuserid'] = $myuserid;
            $_SESSION['login_user'] = $myuserid;

            header("Location: index.php");
        }
    } else {
        $error = "Your login name or password is invalid";
        echo "<script type='text/javascript'>alert('Your Login Name or Password is invalid.')</script>";
    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Your Shirt</title>
        <link href="js&css/bootstrap.min.css" rel="stylesheet">
        <link href="js&css/font-awesome.min.css" rel="stylesheet">
        <link href="js&css/prettyPhoto.css" rel="stylesheet">
        <link href="js&css/price-range.css" rel="stylesheet">
        <link href="js&css/animate.css" rel="stylesheet">
        <link href="js&css/main.css" rel="stylesheet">
        <link href="js&css/responsive.css" rel="stylesheet">
        <!--<link rel="shortcut icon" href="images/ico/favicon.ico">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">-->
    </head><!--/head-->

    <body>
        
        <header id="header"><!--header-->
            <?php include 'common_html/common_header.php' ?>
        </header><!--/header-->
        

        <section id="" style="min-height: 400px"><!--form-->
            <div class="container">
                <div class="row">
                    <div class="col-sm-4 col-sm-offset-1">
                        <div class="login-form"><!--login form-->
                            <h2>Login to your account</h2>
                            <form action="#" method="post">
                                <input type="text" placeholder="email" name="userid" />
                                <input type="password" placeholder="Password" name="userpassword"/>

                                <center><button type="submit" class="btn btn-default" style="width: 100%">Login</button></center>
                                <center><button type="button" class="btn btn-default" onclick="window.location.href = 'register.php'" style="width: 100%">Register</button></center>
                            </form>
                        </div><!--/login form-->
                    </div>

                </div>

            </div>
        </section>


        <footer id="footer"><!--Footer-->
            <?php include 'common_html/common_footer.php' ?>
        </footer><!--/Footer-->



        <script src="js&css/jquery.js"></script>
        <script src="js&css/bootstrap.min.js"></script>
        <script src="js&css/jquery.scrollUp.min.js"></script>
        <script src="js&css/price-range.js"></script>
        <script src="js&css/jquery.prettyPhoto.js"></script>
        <script src="js&css/main.js"></script>
        <script src="js&css/Chart.js"></script>
        <script src="js&css/chart.js-php.js"></script>
    </body>
</html>
