<?php
function generateOrderId($db, $id){
    $select = mysqli_query($db, "SELECT * FROM order_table WHERE status = 1 and order_id = $id");
    if(mysqli_num_rows($select) > 0){
        $newOrderId = $id + 7;
        return generateOrderId($db, $newOrderId);
    }else{
        return $id;
    }
}

include('config.php');


if (!isset($_SESSION['login_user'])) {
    $btn = 'Login';
    $btn_register = 'Register';
    $btn_cart = '';
    $btn_profile = '';
}

if (isset($_SESSION['login_user'])) {
    date_default_timezone_set("Asia/Kuala_Lumpur");
    $user_check = $_SESSION['login_user'];

    $ses_sql = mysqli_query($db, "select user_id from user where user_email='$user_check'");

    $row = mysqli_fetch_array($ses_sql, MYSQLI_ASSOC);

    $login_session = $row['user_id'];
    $btn = 'Logout';
    $btn_register = '';
    $btn_cart = 'My Cart';
    $btn_profile = 'My Profile';
    $orderid = generateOrderId($db, (int)($login_session+date("Ymd")));
}
?>
