<?php
include('adminsession.php');

include_once 'backEnd/admin_Class.php';
$adminClass = new Admin_Class();
$adminClass->mostSearch($db, $searchResult);
$adminClass->mostView($db, $viewResult);
$adminClass->mostOrder($db, $orderResult);
$adminClass->mostOrderTgt($db, $orderTgtResult);
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Your Shirt</title>

        <?php include 'common_html/common_css.php' ?>
    </head><!--/head-->

    <body>
        <header id="header"><!--header-->
            <?php include 'common_html/common_header.php' ?>
        </header><!--/header-->

        <section>
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <?php include 'common_html/common_sideBar.php' ?>
                    </div>
                    <div class="col-sm-9 padding-right"><!-- PS buat sini-->
                        <div class="features_items">
                            <h2 class="title text-center">Reports</h2>
                            <div class="table-responsive cart_info">
                                <div class="panel-heading">
                                    <center>
                                        <select class="form-control" id="reportSelected" style="width: 80%" onchange="viewReport('reportSelected');">
                                        <option value="0">- Select a report -</option>
                                        <option value="1">Most Search Keyword</option>
                                        <option value="2">Most Visited Product</option>
                                        <option value="3">Most Ordered Product</option>
                                        <option value="4">Most Commonly Ordered Together Products</option>
                                        </select>
                                    </center>
                                </div>
                                <div id="1" class="panel-heading" hidden>
                                    <button style="float: right" onclick="closeReport('1');"><i class="fa fa-close"></i></button>
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>Keyword Search By User</th>
                                                <th>Frequency</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                $i = 0;
                                                foreach($searchResult as $row){
                                                    if($i <= 20){
                                                        echo "<tr>"
                                                        . "<td>".$row["keyword"]."</td>"
                                                        . "<td>".$row["times"]."</td>"
                                                        . "</tr>";
                                                        $i++;
                                                    }
                                                }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div id="2" class="panel-heading" hidden>
                                    <button style="float: right" onclick="closeReport('2');"><i class="fa fa-close"></i></button>
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>Most Visited Product</th>
                                                <th>Frequency</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                $i = 0;
                                                foreach($viewResult as $row){
                                                    if($i <= 20){
                                                        echo "<tr>"
                                                        . "<td><a href=\"product_detail.php?rmid=".$row["RM_id"]."&product=".$row["product_name"]."\">".$row["product_name"]."</a></td>"
                                                        . "<td>".$row["view"]."</td>"
                                                        . "</tr>";
                                                        $i++;
                                                    }
                                                }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div id="3" class="panel-heading" hidden>
                                    <button style="float: right" onclick="closeReport('3');"><i class="fa fa-close"></i></button>
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>Most Ordered Product</th>
                                                <th>Size</th>
                                                <th>Frequency</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                $i = 0;
                                                foreach($orderResult as $row){
                                                    if($i <= 20){
                                                        echo "<tr>"
                                                        . "<td><a href=\"product_detail.php?rmid=".$row["RM_id"]."&product=".$row["product_name"]."\">".$row["product_name"]."</a></td>"
                                                        . "<td>".$row["size"]."</td>"
                                                        . "<td>".$row["view"]."</td>"
                                                        . "</tr>";
                                                        $i++;
                                                    }
                                                }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div id="4" class="panel-heading" hidden>
                                    <button style="float: right" onclick="closeReport('4');"><i class="fa fa-close"></i></button>
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>Most Commonly Ordered Together Product</th>
                                                <th>Frequency</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                $i = 0;
                                                foreach($orderTgtResult as $row){
                                                    $id = explode(",", $row["order_tgt"]);
                                                    if($i <= 20){
                                                        echo "<tr>"
                                                        . "<td>";
                                                        for($i=0; $i<count($id); $i++){
                                                            $adminClass->getDetail($db, $id[$i], $data);
                                                            foreach($data as $row2){
                                                                $j = $i+1;
                                                                echo "$j) <a href=\"product_detail.php?rmid=".$row2["RM_id"]."&product=".$row2["product_name"]."\">".$row2["product_name"]."</a><br>";
                                                            }
                                                        }
                                                        echo "</td>"
                                                        . "<td>".$row["COUNT(order_id)"]."</td>"
                                                        . "</tr>";
                                                        $i++;
                                                    }
                                                }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div><!--features_items-->
                    </div>
                </div>
            </div>
        </section>
        <footer id="footer"><!--Footer-->
            <?php include 'common_html/common_footer.php' ?>
        </footer><!--/Footer-->

        <script src="js&css/jquery.js"></script>
        <script src="js&css/bootstrap.min.js"></script>
        <script src="js&css/jquery.scrollUp.min.js"></script>
        <script src="js&css/price-range.js"></script>
        <script src="js&css/jquery.prettyPhoto.js"></script>
        <script src="js&css/main.js"></script>
        <script src="js&css/Chart.js"></script>
        <script src="js&css/chart.js-php.js"></script>
        <script>
            function viewReport(id){
                var report = document.getElementById(id).value;
                if($('#'+report).is(":hidden")){
                    $('#'+report).removeAttr('hidden');
                }
            }
            
            function closeReport(id){
                $('#'+id).attr("hidden",true);
            }
        </script>
        
    </body>    
</html>
